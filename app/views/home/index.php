<div class="container-fluid">
    <div class="bg-container">


        <div class="row">
            <div class="col-lg-4 mx-auto bg-logo" style="height:40vh;">
                <div class="logo">
                    <img src="<?= BASEURL; ?>img/logob.png" alt="MS GLOW BANJARNEGARA">
                </div>
                <div class="banner">
                    <h2>MS GLOW BANJARNEGARA</h2>
                    <h5>user login</h5>
                </div>
            </div>
        </div>
        <div class="row row">
            <div class="col-lg-4 mx-auto form-login" style="height:60vh;">

                <form action="<?= BASEURL; ?>Home/auth" method="post" class="mt-5">

                    <div class="form-group mb-1">
                        <label for="userName" class="login-label mb-0"><small>Username</small></label>
                        <input type="text" name="userName" id="userName" class="form-control" placeholder="username">
                    </div>

                    <div class="form-group mb-1">
                        <label for="authKey" class="login-label mb-0"><small>Kata Sandi</small></label>
                        <input type="password" name="authKey" id="authKey" class="form-control" placeholder="password">
                    </div>
                    <div class="form-group mt-3 d-flex justify-content-center">
                        <input type="submit" value="Login" class="btn btn-warning">
                    </div>

                </form>
                <?php Alert::sankil(); ?>
            </div>
        </div>
    </div>
</div>
<?php $this->view('template/bs4js'); ?>