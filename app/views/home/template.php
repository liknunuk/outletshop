<!-- container: in header -->
<div class="container-fluid">
  <div class="fixed-top">
    <!-- header halaman -->
    <div class="row">
      <div class="col corpid">
        <h5 class="mx-4 mb-0">MS GLOW BANJARNEGARA</h5>
      </div>
    </div>
    <!-- menu utama -->
    <div class="row mt-0 py-0">
      <div class="col">
        <nav class="navbar navbar-expand-lg navbar-light bg-light my-0 py-0">
          <a class="navbar-brand" href="<?= BASEURL; ?>"><i class="fas fa-home" style="48px;"></i></a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
              <li class="nav-item">
                <a class="nav-link" href="<?= BASEURL; ?>stuff">Barang</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="<?= BASEURL; ?>supplyer">Supplyer</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="<?= BASEURL; ?>clients">Klien</a>
              </li>
              <!--
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Dropdown
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="#">Action</a>
                  <a class="dropdown-item" href="#">Another action</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="#">Something else here</a>
                </div>
              </li>
              -->
            </ul>
            <ul class="navbar-nav">
              <li class="nav-item">
                <a class="nav-link" href="<?= BASEURL; ?>"><i class="fas fa-user">&nbsp;logout</i></a>
              </li>
            </ul>
          </div>
        </nav>
      </div>
    </div>
  </div>
  <!-- container: in header -->
</div> <!-- container: in footer -->

<?php $this->view('template/bs4js'); ?>