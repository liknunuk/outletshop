<div class="row row-top">
    <!-- formulir barang -->
    <!-- Columns: idBarang , namaBarang , realStok , hargaPokok -->
    <div class="col">
        <h4>DATA BARANG</h4>
        <div class="form-inline">
            <input type="hidden" name="fmod" value=<?= $data['fmod']; ?>>
            <label class="sr-only" for="idBarang">Kode Barang</label>
            <div class="input-group mb-2 mr-sm-2">
                <div class="input-group-prepend">
                    <div class="input-group-text">Kode Barang</div>
                </div>
                <input autofocus type="text" class="form-control" name="idBarang" id="idBarang" placeholder="Scan Barcode">
            </div>

            <label class="sr-only" for="namaBarang">Nama Barang</label>
            <div class="input-group mb-2 mr-sm-2">
                <div class="input-group-prepend">
                    <div class="input-group-text">Nama Barang</div>
                </div>
                <input type="text" class="form-control" name="namaBarang" id="namaBarang" placeholder="Nama Barang" maxlength='255'>
            </div>

            <label class="sr-only" for="hargaPokok">Harga Pokok</label>
            <div class="input-group mb-2 mr-sm-2">
                <div class="input-group-prepend">
                    <div class="input-group-text">Harga Pokok</div>
                </div>
                <input type="number" class="form-control text-right pr-1" name="hargaPokok" id="hargaPokok" placeholder="Harga Pokok">
            </div>

            <label class="sr-only" for="hargaJual">Harga Jual</label>
            <div class="input-group mb-2 mr-sm-2">
                <div class="input-group-prepend">
                    <div class="input-group-text">Harga Jual</div>
                </div>
                <input type="number" class="form-control text-right pr-1" name="hargaJual" id="hargaJual" placeholder="Harga Jual Tertinggi">
            </div>

            <label class="sr-only" for="katakunci">Cari Barang</label>
            <div class="input-group mb-2 mr-sm-2">
                <div class="input-group-prepend">
                    <div class="input-group-text">Caribarang</div>
                </div>
                <input type="text" class="form-control" name="katakunci" id="katakunci" placeholder="Nama Barang" maxlength="50">
            </div>

        </div>
    </div>
</div>
<div class="row">
    <div class="col table-responsive">
        <span class="alert"></span>
        <table class="table table-sm table-striped">
            <thead>
                <tr class="tableHeader">
                    <th>KODE BARANG</th>
                    <th>NAMA BARANG</th>
                    <th>HARGA POKOK</th>
                    <th>HARGA JUAL</th>
                    <th>PERSEDIAAN</th>
                    <!-- <th>PENDING</th> -->
                    <th><i class="fas fa-gear"></i></th>
                </tr>
            </thead>
            <tbody id="dftBarang">
                <?php foreach ($data['barang'] as $barang) : ?>
                    <tr>
                        <td><?= $barang['idBarang']; ?></td>
                        <td><?= $barang['namaBarang']; ?></td>
                        <td class="text-right">
                            <?= number_format($barang['hargaPokok'], 2, ',', '.'); ?>
                            <span class="ngumpet"><?= $barang['hargaPokok']; ?></span>
                        </td>
                        <td class="text-right">
                            <?= number_format($barang['hargaJual'], 2, ',', '.'); ?>
                            <span class="ngumpet"><?= $barang['hargaJual']; ?></span>
                        </td>
                        <td class="text-right"><?= $barang['realStok']; ?></td>
                        <!-- <td class="text-right"><?= $barang['pending']; ?></td> -->
                        <td class="text-center">
                            <a href="javascript:void(0)" class="chg"><i class="fas fa-edit"></i></a>
                            <a href="javascript:void(0)" class="del"><i class="fas fa-times text-danger"></i></a>
                            <a href="javascript:void(0)" class="exc"><i class="fas fa-cash-register"></i></a>
                            <a href="javascript:void(0)" class="rst"><i class="fas fa-redo text-warning"></i></a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <div class="d-flex justify-content-center">
            <button class="btn btn-success prev"><i class="fas fa-angle-double-left"></i></button>
            <span class="mx-2 mt-2 pageNumber"><?= $data['pn']; ?></span>
            <button class="btn btn-success next"><i class="fas fa-angle-double-right"></i></button>
        </div>
    </div>
</div>

<?php $this->view('template/bs4js'); ?>
<script>
    $('#idBarang').on('change', function() {
        $('#namaBarang').focus();
    })

    $('#namaBarang').on('keypress', function(e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            $('#hargaPokok').focus();
        }
    })

    $('#hargaPokok').on('keypress', function(e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            $('#hargaJual').focus();
        }
    })

    $('#hargaJual').on('keypress', function(e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            $.post('<?= BASEURL; ?>Barang/simpanBarang', {
                fmod: $("input[name='fmod']").val(),
                idBarang: $('#idBarang').val().trim(),
                namaBarang: $('#namaBarang').val(),
                hargaPokok: $('#hargaPokok').val(),
                hargaJual: $('#hargaJual').val(),
            }, function(res) {
                if (res == '1') {
                    $('.alert').addClass('text-success');
                    $('.alert').text('Data berhasil disimpan');
                    setTimeout(function() {
                        $('.alert').text('')
                    }, 2000);
                    emptyForm();
                    loadBarang();
                } else {
                    $('.alert').addClass('text-danger');
                    $('.alert').text('Data berhasil disimpan');
                    setTimeout(function() {
                        $('.alert').text('')
                    }, 2000);
                }
            })
        }
    })

    $('#dftBarang').on('click', '.chg', function() {
        let idBarang = $(this).parent('td').parent('tr').children('td:nth-child(1)').text();
        let namaBarang = $(this).parent('td').parent('tr').children('td:nth-child(2)').text();
        let hargaPokok = $(this).parent('td').parent('tr').children('td:nth-child(3)').children('span').text();
        let hargaJual = $(this).parent('td').parent('tr').children('td:nth-child(4)').children('span').text();
        $('#idBarang').val(idBarang);
        $('#namaBarang').val(namaBarang);
        $('#hargaPokok').val(hargaPokok);
        $('#hargaJual').val(hargaJual);
        $("input[name='fmod']").val('chg');
    })

    $('#dftBarang').on('click', '.del', function() {
        let idBarang = $(this).parent('td').parent('tr').children('td:nth-child(1)').text();
        let tenan = confirm('Barang akan dihapus');
        if (tenan == true) {
            $.post('<?= BASEURL; ?>Barang/afkir', {
                idBarang: idBarang
            }, function(res) {
                if (res == "1") {
                    loadBarang();
                    $('.alert').text('Data berhasil dihapus');
                    setTimeout(function() {
                        $('.alert').text('')
                    }, 2000);
                    emptyForm();
                    loadBarang();
                }
            })
        }
    })

    $('#dftBarang').on('click', '.exc', function() {
        let idBarang = $(this).parent('td').parent('tr').children('td:nth-child(1)').text();
        window.location.href = "<?= BASEURL; ?>Barang/soldHistory/" + idBarang.replace(/\ /g, "-");
    })

    var pn = <?= $data['pn']; ?>;
    $('.prev').click(function() {
        let np = pn - 1;
        if (pn == 1) {
            window.location.href = "<?= BASEURL; ?>Barang/";
        } else {
            window.location.href = "<?= BASEURL; ?>Barang/" + np;
        }
    })

    $('.next').click(function() {
        let np = pn + 1;
        window.location.href = "<?= BASEURL; ?>Barang/" + np;
    })

    $('#katakunci').keypress(function(e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            let keyword = this.value,
                namabarang = keyword.replace(/\ /g, "-");

            $.getJSON(
                '<?= BASEURL; ?>Barang/cari/' + namabarang,
                function(res) {
                    $('#dftBarang tr').remove();
                    $.each(res, function(i, data) {
                        $('#dftBarang').append(`
                    <tr>
                    <td>${data.idBarang}</td>
                        <td>${data.namaBarang}</td>
                        <td class="text-right">
                        ${parseInt(data.hargaPokok).toLocaleString('id-ID')}
                        <span class="ngumpet">${data.hargaPokok}</span>
                        </td>
                        <td class="text-right">
                        ${parseInt(data.hargaJual).toLocaleString('id-ID')}
                        <span class="ngumpet">${data.hargaJual}</span>
                        </td>
                        <td class="text-right">${data.realStok}</td>
                        <td>
                            <a href="javascript:void(0)" class="chg"><i class="fas fa-edit"></i></a>
                            <a href="javascript:void(0)" class="del"><i class="fas fa-times text-danger"></i></a>
                        </td>
                    </tr>
                    `)
                    })
                }
            );
        }
    })

    function emptyForm() {
        $('#idBarang').focus();
        $('#idBarang').val('');
        $('#namaBarang').val('');
        $('#hargaPokok').val('');
        $('#hargaJual').val('');
    }

    $('.rst').click(function() {
        let idBarang = $(this).parent('td').parent('tr').children('td:nth-child(1)').text().trim();
        let realStok = prompt("Jumlah Stok Terkini", 0);
        if (realStok == null || realStok == "") {
            text = "Reset Stok Batal";
        } else {
            $.post(
                "<?= BASEURL; ?>Barang/restoking", {
                    idBarang: idBarang,
                    realStok: realStok
                },
                function(res) {
                    console.log(res);
                    if (res == "1") {
                        alert("Stok telah direset");
                        location.reload();
                    }
                }
            )
        }

    })

    function loadBarang() {
        $.getJSON(
            "<?= BASEURL; ?>Barang/loBarang/<?= $data['pn']; ?>",
            function(res) {
                $('#dftBarang tr').remove();
                $.each(res, function(i, data) {
                    $('#dftBarang').append(`
                    <tr>
                        <td>${data.idBarang}</td>
                        <td>${data.namaBarang}</td>
                        <td class="text-right">
                        ${parseInt(data.hargaPokok).toLocaleString('id-ID')}
                        <span class="ngumpet">${data.hargaPokok}</span>
                        </td>
                        <td class="text-right">
                        ${parseInt(data.hargaJual).toLocaleString('id-ID')}
                        <span class="ngumpet">${data.hargaJual}</span>
                        </td>
                        <td class="text-right">${data.realStok}</td>
                        <td class="text-center">
                            <a href="javascript:void(0)" class="chg"><i class="fas fa-edit"></i></a>
                            <a href="javascript:void(0)" class="del"><i class="fas fa-times text-danger"></i></a>
                            <a href="javascript:void(0)" class="exc"><i class="fas fa-cash-register"></i></a>
                        </td>
                    </tr>
                    `)
                })
            }
        );
    }
</script>