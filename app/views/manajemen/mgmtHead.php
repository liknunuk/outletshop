<!-- container: in header -->
<div class="container-fluid">
    <div class="fixed-top">
        <!-- header halaman -->
        <div class="row">
            <div class="col-sm-1 corpid text-center">
                <img src="<?= BASEURL; ?>img/logo.png" alt="msglow">
            </div>
            <div class="col-sm-11 corpid">
                <h5 class="mx-4 mb-0">MS Glow Banjarnegara</h5>
            </div>
        </div>
        <!-- menu utama -->
        <div class="row mt-0 py-0">
            <div class="col">
                <nav class="navbar navbar-expand-lg navbar-light bg-light my-0 py-0">
                    <a class="navbar-brand" href="<?= BASEURL; ?>Sales"><i class="fas fa-home" style="48px;"></i></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item">
                                <a class="nav-link" href="<?= BASEURL; ?>Barang">Barang</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="<?= BASEURL; ?>Supplyer">Supplyer</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="<?= BASEURL; ?>Clients">Klien</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="<?= BASEURL; ?>Barang/pembelian">Purchasing</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="<?= BASEURL; ?>Users">Staff</a>
                            </li>
                            <!--
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Dropdown
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="#">Action</a>
                  <a class="dropdown-item" href="#">Another action</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="#">Something else here</a>
                </div>
              </li>
              -->
                        </ul>
                        <ul class="navbar-nav">
                            <li class="nav-item">
                                <a class="nav-link" href="<?= BASEURL; ?>Home/logout"><i class="fas fa-user">&nbsp;logout</i></a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
    <!-- container: in header -->