<div class="row row-top">
    <!-- formulir Client -->
    <!-- Columns: namaClient, telepon, tipeClient, idClient -->
    <div class="col">
        <h4>DATA SUPPLYER</h4>
        <div class="form-inline">
            <input type="hidden" name="fmod" value="<?= $data['fmod']; ?>">

            <label class="sr-only" for="idClient">ID Klien</label>
            <div class="input-group mb-2 mr-sm-2">
                <div class="input-group-prepend">
                    <div class="input-group-text">ID Klien</div>
                </div>
                <input autofocus type="text" class="form-control" name="idClient" id="idClient" placeholder="Nama Klien">
            </div>

            <label class="sr-only" for="namaClient">Nama Klien</label>
            <div class="input-group mb-2 mr-sm-2">
                <div class="input-group-prepend">
                    <div class="input-group-text">Nama Klien</div>
                </div>
                <input type="text" class="form-control" name="namaClient" id="namaClient" placeholder="Nama Klien">
            </div>

            <label class="sr-only" for="tipeClient">Kategori</label>
            <div class="input-group mb-2 mr-sm-2">
                <div class="input-group-prepend">
                    <div class="input-group-text">Kategori</div>
                </div>
                <select class="form-control" name="tipeClient" id="tipeClient">
                    <option value="Member">Member</option>
                    <option value="Reseller">Reseller</option>
                    <option value="User">User</option>
                </select>
            </div>

            <label class="sr-only" for="telepon">Telepon</label>
            <div class="input-group mb-2 mr-sm-2">
                <div class="input-group-prepend">
                    <div class="input-group-text">Telepon</div>
                </div>
                <input type="text" class="form-control" name="telepon" id="telepon" placeholder="No. Telp/HP">
            </div>

            <label class="sr-only" for="katakunci">Cari Klien</label>
            <div class="input-group mb-2 mr-sm-2">
                <div class="input-group-prepend">
                    <div class="input-group-text">Cari Klien</div>
                </div>
                <input type="text" class="form-control" name="katakunci" id="katakunci" placeholder="Nama Klien">
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col table-responsive">
        <span class="alert"></span>
        <table class="table table-sm table-striped">
            <thead>
                <tr class="tableHeader">
                    <th>NOMOR</th>
                    <th>NAMA KLIEN</th>
                    <th>NOMOR TELEPON</th>
                    <th>KATEGORI</th>
                    <th>PENDING<br />BARANG</th>
                    <th width="150px"><i class="fas fa-gear"></i></th>
                </tr>
            </thead>
            <tbody id="dftClients">
                <?php
                $nomor = ($data['pn'] - 1) * rows + 1;
                $basenomor = $nomor;
                foreach ($data['clients'] as $client) :
                ?>
                    <tr>
                        <td class="text-right mr-2">
                            <?= $nomor++; ?>
                            <span class="ngumpet"><?= $client['idClient']; ?></span>
                        </td>
                        <td><?= $client['namaClient']; ?></td>
                        <td><?= $client['telepon']; ?></td>
                        <td><?= $client['tipeClient']; ?></td>
                        <td class="text-center"><?php echo $client['pending'] == null ? "0" : $client['pending']; ?></td>
                        <td class="text-center">
                            <a href="javascript:void(0)" class="chg"><i class="fas fa-edit"></i></a>
                            <a href="javascript:void(0)" class="del"><i class="fas fa-times text-danger"></i></a>
                            <a href="javascript:void(0)" class="trx"><i class="fas fa-cash-register text-dark"></i></a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <div class="d-flex justify-content-center">
            <button class="btn btn-success prev"><i class="fas fa-angle-double-left"></i></button>
            <span class="mx-2 mt-2 pageNumber"><?= $data['pn']; ?></span>
            <button class="btn btn-success next"><i class="fas fa-angle-double-right"></i></button>
        </div>
    </div>
</div>

<?php $this->view('template/bs4js'); ?>
<script>
    $('#idClient').on('keypress', function(e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            $('#namaClient').focus();
        }
    })

    $('#namaClient').on('keypress', function(e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            $('#tipeClient').focus();
        }
    })

    $('#tipeClient').on('change', function() {
        $('#telepon').focus();
    })

    $('#telepon').on('keypress', function(e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            $.post('<?= BASEURL; ?>Clients/simpan', {
                fmod: $("input[name='fmod']").val(),
                idClient: $('#idClient').val(),
                namaClient: $('#namaClient').val(),
                tipeClient: $('#tipeClient').val(),
                telepon: $('#telepon').val(),
            }, function(res) {
                console.log(res);
                if (res == '1') {
                    $('.alert').addClass('text-success');
                    $('.alert').text('Data berhasil disimpan');
                    setTimeout(function() {
                        $('.alert').text('')
                    }, 2000);
                    emptyForm();
                    loadClients();
                    $("input[name='fmod']").val('new');
                } else {
                    $('.alert').addClass('text-danger');
                    $('.alert').text('Data berhasil disimpan');
                    setTimeout(function() {
                        $('.alert').text('')
                    }, 2000);
                }
            })
        }
    })

    $('#dftClients').on('click', '.chg', function() {
        let idClient = $(this).parent('td').parent('tr').children('td:nth-child(1)').children('span').text();
        let namaClient = $(this).parent('td').parent('tr').children('td:nth-child(2)').text();
        let telepon = $(this).parent('td').parent('tr').children('td:nth-child(3)').text();
        let tipeClient = $(this).parent('td').parent('tr').children('td:nth-child(4)').text();
        $('#idClient').val(idClient);
        $('#namaClient').val(namaClient);
        $('#telepon').val(telepon);
        $('#tipeClient').val(tipeClient);
        $("input[name='fmod']").val('chg');
    })

    $('#dftClients').on('click', '.del', function() {
        let idClient = $(this).parent('td').parent('tr').children('td:nth-child(1)').children('span').text();
        let tenan = confirm('Client akan dihapus');
        if (tenan == true) {
            $.post('<?= BASEURL; ?>Clients/afkir', {
                idClient: idClient
            }, function(res) {
                if (res == "1") {
                    loadClients();
                    $('.alert').text('Data berhasil dihapus');
                    setTimeout(function() {
                        $('.alert').text('')
                    }, 2000);
                    emptyForm();
                    loadClients();
                }
            })
        }
    })
    var pn = <?= $data['pn']; ?>;
    $('.prev').click(function() {
        let np = pn - 1;
        if (pn == 1) {
            window.location.href = "<?= BASEURL; ?>Clients/";
        } else {
            window.location.href = "<?= BASEURL; ?>Clients/" + np;
        }
    })

    $('.next').click(function() {
        let np = pn + 1;
        window.location.href = "<?= BASEURL; ?>Clients/" + np;
    })

    $('#katakunci').keypress(function(e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            let keyword = this.value,
                namaSupplyer = keyword.replace(/\ /g, "-"),
                nomor = 1;

            $.getJSON(
                '<?= BASEURL; ?>Clients/cari/' + namaSupplyer,
                function(res) {
                    $('#dftClients tr').remove();
                    $.each(res, function(i, data) {
                        $('#dftClients').append(`
                        <tr>
                        <td>${nomor++}<span class='ngumpet'>${data.idClient}</span></td>
                        <td>${data.namaClient}</td>
                        <td>${data.telepon}</td>
                        <td>${data.tipeClient}</td>
                        <td class="text-center">
                            <a href="javascript:void(0)" class="chg"><i class="fas fa-edit"></i></a>
                            <a href="javascript:void(0)" class="del"><i class="fas fa-times text-danger"></i></a>
                            <a href="javascript:void(0)" class="trx"><i class="fas fa-cash-register text-dark"></i></a>
                        </td>
                    </tr>
                    `)
                    })
                }
            );
        }
    })

    function emptyForm() {
        $('#idClient').val('');
        $('#idClient').focus();
        $('#namaClient').val('');
        $('#telepon').val('');
        $('#tipeClient[value="User"]').prop('selected', true);
    }

    $("#dftClients").on('click', '.trx', function() {
        let idClient = $(this).parent('td').parent('tr').children('td:nth-child(1)').children('span').text();
        window.location.href = '<?= BASEURL; ?>Clients/trxes/' + idClient;
    })

    function loadClients() {
        $.getJSON(
            "<?= BASEURL; ?>Clients/loClients/<?= $data['pn']; ?>",
            function(res) {
                $('#dftClients tr').remove();
                let nomor = <?= $basenomor; ?>;
                $.each(res, function(i, data) {
                    $('#dftClients').append(`
                    <tr>
                        <td>${nomor++}<span class='ngumpet'>${data.idClient}</span></td>
                        <td>${data.namaClient}</td>
                        <td>${data.telepon}</td>
                        <td>${data.tipeClient}</td>
                        <td>
                            <a href="javascript:void(0)" class="chg"><i class="fas fa-edit"></i></a>
                            <a href="javascript:void(0)" class="del"><i class="fas fa-times text-danger"></i></a>
                        </td>
                    </tr>
                    `)
                })
            }
        );
    }
</script>