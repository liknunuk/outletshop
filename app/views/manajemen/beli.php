<div class="row row-top mb-3">
    <div class="col-lg-6">
        <h4>PEMBELIAN BARANG</h4>
    </div>
    <div class="col-lg-3">
        <select id="srcCol" class="form-control">
            <option value="">Cari Pembelian berdasar</option>
            <option value="idInvoice">No. Invoice</option>
            <option value="idSupplyer">Supplyer</option>
            <option value="idBarang">Nama Barang</option>
        </select>
    </div>
    <div class="col-lg-2">
        <input type="text" id="srcVal" class="form-control" placeholder="tuliskan data pencarian" list="loFilter">
        <datalist id="loFilter"></datalist>
    </div>
    <div class="col-lg-1">
        <button class="btn btn-success" id="srcGo">Cari!</button>
    </div>
</div>
<div class="row">
    <!-- Tabel Pembelian -->
    <!-- // Columns: tanggal, idSupplyer , idBarang , quantity , hargaBeli , idInvoice, idxItem -->
    <div class="col-md-10">
        <div id="alert"></div>
        <table class="table table-sm table-striped">
            <thead class="tableHeader">
                <tr>
                    <th>Tanggal</th>
                    <th>Supplyer</th>
                    <th>Barang</th>
                    <th>Banyaknya</th>
                    <th>Harga Beli</th>
                    <th>No. Invoice</th>
                    <th><i class="fas fa-tasks"></i></th>
                </tr>
            </thead>
            <tbody id="dftPembelian"></tbody>
        </table>
    </div>
    <!-- Form Pembelian -->
    <div class="col-md-2">
        <button class="btn btn-success btn-sm form-control" id="newInvoice">Tambah Pembelian</button>
        <div>
            <input type="hidden" id="fmod" value="<?= $data['fmod']; ?>">
            <input type="hidden" id="idxItem" value="">
            <div class="form-group mb-0">
                <label for="idInvoice" class="mb-0"><small>No. Invoice</small></label>
                <input type="text" id="idInvoice" class="form-control form-control-sm" readonly placeholder="Nomor Nota/Invoice">
            </div>

            <div class="form-group mb-0">
                <label for="tanggal" class="mb-0"><small>Tanggal Invoice</small></label>
                <input type="date" id="tanggal" class="form-control form-control-sm" readonly placeholder="tanggal pembelian" value="<?= date('Y-m-d'); ?>">
            </div>

            <div class="form-group mb-0">
                <label for="idSupplyer" class="mb-0"><small>Supplyer</small></label>
                <input type="text" id="idSupplyer" class="form-control form-control-sm" readonly placeholder="Cari Supplyer" list="loSupplyer">
                <datalist id="loSupplyer"></datalist>
            </div>

            <div class="form-group mb-0">
                <label for="idBarang" class="mb-0"><small>Kode Barang</small></label>
                <input type="text" id="idBarang" class="form-control form-control-sm" list="loBarang" placeholder=>
                <datalist id="loBarang"></datalist>
            </div>

            <div class="form-group mb-0">
                <label for="quantity" class="mb-0"><small>Banyaknya</small></label>
                <input type="number" id="quantity" class="form-control form-control-sm" maxlength="3">
            </div>

            <div class="form-group mb-0">
                <label for="hargaBeli" class="mb-0"><small>Harga</small></label>
                <input type="number" id="hargaBeli" class="form-control form-control-sm text-right">
            </div>
        </div>
    </div>
</div>

<?php $this->view('template/bs4js'); ?>
<script>
    // buka pembelian terakhir
    $(document).ready(function() {
        loadPembelian();
    })

    // buka form data invoice
    $('#newInvoice').click(function() {
        $('#tanggal').prop('readonly', false);
        $('#idSupplyer').prop('readonly', false);
        $('#idSupplyer').val('');
        $('#idInvoice').prop('readonly', false);
        $('#idInvoice').val('');
        $('#idInvoice').focus();
        $('#fmod').val('new');
        $('#idxItem').val('');
        $('#idBarang').val('');
        $('#quantity').val('');
        $('#hargaBeli').val('');
    })

    // Kunci data invoice
    $('#idSupplyer').keypress(function(e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            $('#tanggal').prop('readonly', true);
            $('#idSupplyer').prop('readonly', true);
            $('#idInvoice').prop('readonly', true);
            $('#idBarang').focus();
        }
    })

    // Cari Supplyer
    $('#idSupplyer').keyup(function() {
        let namaSupplyer = this.value;
        if (namaSupplyer.length >= 3) {
            let supplyer = namaSupplyer.replace(/\ /g, "-");
            $.getJSON(
                '<?= BASEURL; ?>Supplyer/cari/' + supplyer,
                function(res) {
                    $('#loSupplyer option').remove();
                    $.each(res, function(i, data) {
                        $('#loSupplyer').append(`<option value="${data.idSupplyer}-${data.namaSupplyer}">${data.namaSupplyer}</option>`)
                    })
                }
            )
        }
    })

    // Pindah ke tanggal
    $('#idInvoice').change(function() {
        $('#tanggal').focus();
    })

    // Pindah ke supplyer
    $('#tanggal').change(function() {
        $('#idSupplyer').focus();
    })

    // Pindah ke quantity
    $('#idBarang').change(function() {
        $('#quantity').focus();
    })

    $('#idBarang').keypress(function(e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            $('#quantity').focus();
        }
    })

    // pindah ke harga
    $('#quantity').keypress(function(e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            $('#hargaBeli').focus();
        }
    });

    // Simpan data pembelian
    $('#hargaBeli').keypress(function(e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            let idSupplyer = $('#idSupplyer').val().split('-');
            $.post(
                "<?= BASEURL; ?>Barang/savePembelian", {
                    tanggal: $('#tanggal').val(),
                    idSupplyer: idSupplyer[0],
                    idBarang: $('#idBarang').val(),
                    quantity: $('#quantity').val(),
                    hargaBeli: $('#hargaBeli').val(),
                    idInvoice: $('#idInvoice').val(),
                    fmod: $('#fmod').val(),
                    idxItem: $('#idxItem').val(),
                },
                function(res) {
                    console.log(res);
                    if (res == "1") {
                        emptyForm();
                        loadPembelian();
                    }
                }
            )
        }
    })

    function emptyForm() {
        $('#idBarang').val('');
        $('#quantity').val('');
        $('#hargaBeli').val('');
        $('#idBarang').focus();
    }

    $("#dftPembelian").on('click', '.chg', function() {
        let idxItem = striper(this.id),
            idInvoice = $(this).parent('td').parent('tr').children('td:nth-child(6)').text(),
            tanggal = $(this).parent('td').parent('tr').children('td:nth-child(1)').children('span').text(),
            idSupplyer = $(this).parent('td').parent('tr').children('td:nth-child(2)').children('span').text(),
            idBarang = $(this).parent('td').parent('tr').children('td:nth-child(3)').children('span').text(),
            quantity = $(this).parent('td').parent('tr').children('td:nth-child(4)').text(),
            hargaBeli = $(this).parent('td').parent('tr').children('td:nth-child(5)').text();

        $('#idxItem').val(idxItem[1]);
        $('#idInvoice').val(idInvoice);
        $('#tanggal').val(tanggal);
        $('#idSupplyer').val(idSupplyer);
        $('#idBarang').val(idBarang);
        $('#idBarang').focus();
        $('#quantity').val(quantity);
        $('#hargaBeli').val(hargaBeli);
        $('#fmod').val(idxItem[0]);

    })

    $("#dftPembelian").on('click', '.del', function() {
        let tenan = confirm('Pembelian barang dibatalkan?');
        if (tenan == true) {
            let idxItem = striper(this.id);
            $.post("<?= BASEURL; ?>Barang/rasidatuku", {
                idxItem: idxItem[1]
            }, function(res) {
                if (res == "1") {
                    $('#alert').addClass('text-success');
                    $('#alert').text('Data berhasil dihapus');
                    setTimeout(function() {
                        $('#alert').text('');
                    }, 2000);
                } else {
                    $('#alert').text('Data gagal dihapus');
                    $('#alert').addClass('text-danger');
                    setTimeout(function() {
                        $('#alert').text('');
                    }, 2000);
                }
                loadPembelian();
            })
        }
    })

    $('#dftPembelian').on('click', '.idSup', function() {
        let idSupplyer = striper(this.id);
        // alert(idSupplyer[1]);
        $.getJSON(
            '<?= BASEURL; ?>Barang/dari/' + idSupplyer[1],
            function(res) {
                $('#dftPembelian tr').remove();
                $.each(res, function(i, data) {
                    appendMe(data);
                })
            }
        )
    })
    $('#dftPembelian').on('click', '.idBar', function() {
        let idBarang = striper(this.id);
        // alert(idBarang[1]);
        $.getJSON(
            '<?= BASEURL; ?>Barang/byBarang/' + idBarang[1],
            function(res) {
                $('#dftPembelian tr').remove();
                $.each(res, function(i, data) {
                    appendMe(data);
                })
            }
        )
    })
    $('#dftPembelian').on('click', '.idInv', function() {
        let idInvoice = this.innerHTML;
        // alert(idInvoice);
        $.getJSON(
            '<?= BASEURL; ?>Barang/byInvoice/' + idInvoice,
            function(res) {
                $('#dftPembelian tr').remove();
                $.each(res, function(i, data) {
                    appendMe(data);
                })
            }
        )
    })

    // Pencarian Data Pembelian
    $('#srcCol').change(function() {
        let col = this.value;
        let scv = $('#srcVal');
        if (col == 'idInvoice') {
            scv.prop('placeholder', 'Tuliskan Nomor Invoice');
        }

        if (col == 'idSupplyer') {
            scv.prop('placeholder', 'Tuliskan Nama Supplyer');
        }

        if (col == 'idBarang') {
            scv.prop('placeholder', 'Tuliskan Nama Barang');
        }
        scv.focus();

    })

    $('#srcVal').keyup(function() {
        let val = this.value.replace(/\ /g, "-");
        let col = $('#srcCol').val();
        if (val.length >= 4) {
            $.getJSON(
                `<?= BASEURL; ?>Barang/filterbeli/${col}/${val}`,
                function(res) {
                    // console.log(res);
                    $("#loFilter option").remove();
                    $.each(res, function(i, data) {
                        $('#loFilter').append(`<option value='${data.opt}'>${data.val}</option>`);
                    })
                }
            )
        }
    })

    $('#srcGo').click(function() {
        let col = $('#srcCol').val();
        let val = $('#srcVal').val().replace(/\ /g, "-");
        $.getJSON(
            `<?= BASEURL; ?>Barang/cadapem/${col}/${val}`,
            function(res) {
                $("#dftPembelian tr").remove();
                $.each(res, function(i, data) {
                    appendMe(data);
                })
            }
        )
    })

    function striper(data) {
        return (data.split('_'));
    }

    function appendMe(data) {
        $('#dftPembelian').append(`
            <tr>
                <td>
                    ${data.tgbeli}
                    <span class="ngumpet">${data.tanggal}</span>
                </td>
                <td>
                <a href="javascript:void(0)"class='idSup' id='sup_${data.idSupplyer}'> ${data.namaSupplyer}</a>
                <span class="ngumpet">${data.idSupplyer}-${data.namaSupplyer}</span>
                </td>
                <td><a href="javascript:void(0)" class='idBar' id='bar_${data.idBarang}'>${data.namaBarang}</a><span class='ngumpet'>${data.idBarang}</span></td>
                <td class='text-right'>${data.quantity}</td>
                <td class='text-right'>${data.hargaBeli}</td>
                <td>
                    <a href="javascript:void(0)"class='idInv'>${data.idInvoice}</a>
                </td>
                <td>
                <a href="javascript:void(0)" class="chg" id="chg_${data.idxItem}"><i class="fas fa-edit text-success"></i></a>
                <a href="javascript:void(0)" class="del" id="del_${data.idxItem}"><i class="fas fa-times text-danger"></i></a>
                </td>
            </tr>
        `);
    }

    function loadPembelian() {
        $.getJSON(
            "<?= BASEURL; ?>Barang/lastkulak",
            function(res) {
                $('#dftPembelian tr').remove();
                $.each(res, function(i, data) {
                    $('#dftPembelian').append(`
                    <tr>
                        <td>
                            ${data.tgbeli}
                            <span class="ngumpet">${data.tanggal}</span>
                        </td>
                        <td>
                        <a href="javascript:void(0)"class='idSup' id='sup_${data.idSupplyer}'> ${data.namaSupplyer}</a>
                        <span class="ngumpet">${data.idSupplyer}-${data.namaSupplyer}</span>
                        </td>
                        <td><a href="javascript:void(0)" class='idBar' id='bar_${data.idBarang}'>${data.namaBarang}</a><span class='ngumpet'>${data.idBarang}</span></td>
                        <td class='text-right'>${data.quantity}</td>
                        <td class='text-right'>${data.hargaBeli}</td>
                        <td><a href="javascript:void(0)"class='idInv'>${data.idInvoice}</a></td>
                        <td>
                        <a href="javascript:void(0)" class="chg" id="chg_${data.idxItem}"><i class="fas fa-edit text-success"></i></a>
                        <a href="javascript:void(0)" class="del" id="del_${data.idxItem}"><i class="fas fa-times text-danger"></i></a>
                        </td>
                    </tr>
                    `)
                })
            }
        )
    }
</script>