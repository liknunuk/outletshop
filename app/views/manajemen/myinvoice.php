<?php

/**
 * Array ( [invoice] => Array ( [idInvoice] => 000004 [tanggal] => 2021-08-08 [idClient] => R0286MD0001 [grandTotal] => 50330 [bayar] => 400000 [kembali] => 349670 [ongkir] => 50000 [tgInvoice] => 08/08/2021 [namaClient] => Anggita Pratama [tipeClient] => Member ) 
 * 
 * [barang] => Array ( [0] => Array ( [idxPenjualan] => 00000007 [idInvoice] => 000004 [idBarang] => msg001 [quantity] => 2 [hargaBeli] => 50000 [hargaJual] => 60000 [namaBarang] => Msglow-001 ) [1] => Array ( [idxPenjualan] => 00000008 [idInvoice] => 000004 [idBarang] => msg002 [quantity] => 3 [hargaBeli] => 55000 [hargaJual] => 70000 [namaBarang] => Msglow-002 ) ) ) 
 */
?>
<table class="table table-sm">
    <tbody>
        <tr>
            <td>Nomor Invoice</td>
            <td><?= $data['invoice']['idInvoice']; ?></td>
        </tr>
        <tr>
            <td>Tanggal Invoice</td>
            <td><?= $data['invoice']['tanggal']; ?></td>
        </tr>
        <tr>
            <td>Pelanggan</td>
            <td><?= $data['invoice']['namaClient']; ?> ( <?= $data['invoice']['idClient']; ?> )</td>
        </tr>


    </tbody>
</table>
<br>
<table class="table table-sm mt-3">
    <tbody>

        <?php
        $subTotal = 0;
        foreach ($data['barang'] as $barang) : ?>
            <tr>
                <td class='text-right'><?= $barang['quantity']; ?> x </td>
                <td><?= $barang['namaBarang']; ?></td>
                <td>@ <?= number_format($barang['hargaJual'], 0, ',', '.'); ?></td>
                <td class="text-right">
                    <?php
                    $jumlah = $barang['hargaJual'] * $barang['quantity'];
                    echo number_format($jumlah, 0, ',', '.');
                    ?>
                </td>
            </tr>
        <?php
            $subTotal += $jumlah;
        endforeach;
        ?>
        <tr>
            <td class="text-right" colspan='3'>Jumlah Total</td>
            <td class="text-right"><?= number_format($subTotal, 0, ',', '.'); ?></td>
        </tr>
        <tr>
            <td class="text-right" colspan='3'>Ongkos Kirim</td>
            <td class="text-right"><?= number_format($data['invoice']['ongkir'], 0, ',', '.'); ?></td>
        </tr>
        <tr>
            <td class="text-right" colspan='3'>Total Tagihan</td>
            <td class="text-right"><?= number_format($data['invoice']['grandTotal'], 0, ',', '.'); ?></td>
        </tr>
        <tr>
            <td colspan="4">&nbsp;</td>
        </tr>
        <tr>
            <td class="text-right" colspan='2'>Ekspedisi</td>
            <td colspan="2"><?= $data['invoice']['ekspedisi']; ?></td>
        </tr>
        <tr>
            <td class="text-right" colspan='2'>Nomor Resi</td>
            <td colspan="2"><span id="noResi"><?= $data['invoice']['waybill']; ?></span><a href="#" class="track">Track</a></td>
        </tr>
    </tbody>
</table>