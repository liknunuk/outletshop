<div class="row row-top">
    <!-- formulir barang -->
    <!-- Columns: namaSupplyer , telepon, idSupplyer -->
    <div class="col">
        <h4>DATA SUPPLYER</h4>
        <div class="form-inline">
            <input type="hidden" name="fmod" value=<?= $data['fmod']; ?>>
            <label class="sr-only" for="idSupplyer">ID Supplyer</label>
            <div class="input-group mb-2 mr-sm-2">
                <div class="input-group-prepend">
                    <div class="input-group-text">ID Supplyer</div>
                </div>
                <input type="text" class="form-control" name="idSupplyer" id="idSupplyer" placeholder="ID Supplyer" readonly>
            </div>

            <label class="sr-only" for="namaSupplyer">Nama Supplyer</label>
            <div class="input-group mb-2 mr-sm-2">
                <div class="input-group-prepend">
                    <div class="input-group-text">Nama Supplyer</div>
                </div>
                <input autofocus type="text" class="form-control" name="namaSupplyer" id="namaSupplyer" placeholder="Nama Barang" maxlength='50'>
            </div>

            <label class="sr-only" for="telepon">Telepon</label>
            <div class="input-group mb-2 mr-sm-2">
                <div class="input-group-prepend">
                    <div class="input-group-text">Telepon</div>
                </div>
                <input type="text" class="form-control" name="telepon" id="telepon" placeholder="No. Telp/HP">
            </div>

            <label class="sr-only" for="katakunci">Cari Supplyer</label>
            <div class="input-group mb-2 mr-sm-2">
                <div class="input-group-prepend">
                    <div class="input-group-text">Cari Supplyer</div>
                </div>
                <input type="text" class="form-control" name="katakunci" id="katakunci" placeholder="Nama Supplyer">
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col table-responsive">
        <span class="alert"></span>
        <table class="table table-sm table-striped">
            <thead>
                <tr class="tableHeader">
                    <th>NOMOR</th>
                    <th>NAMA SUPPLYER</th>
                    <th>NOMOR TELEPON</th>
                    <th><i class="fas fa-gear"></i></th>
                </tr>
            </thead>
            <tbody id="dftSupplyer">
                <?php
                $nomor = ($data['pn'] - 1) * rows + 1;
                $basenomor = $nomor;
                foreach ($data['supplyer'] as $supplyer) :
                ?>
                    <tr>
                        <td class="text-right mr-2">
                            <?= $nomor++; ?>
                            <span class="ngumpet"><?= $supplyer['idSupplyer']; ?></span>
                        </td>
                        <td><?= $supplyer['namaSupplyer']; ?></td>
                        <td><?= $supplyer['telepon']; ?></td>

                        <td>
                            <a href="javascript:void(0)" class="chg"><i class="fas fa-edit"></i></a>
                            <a href="javascript:void(0)" class="del"><i class="fas fa-times text-danger"></i></a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <div class="d-flex justify-content-center">
            <button class="btn btn-success prev"><i class="fas fa-angle-double-left"></i></button>
            <span class="mx-2 mt-2 pageNumber"><?= $data['pn']; ?></span>
            <button class="btn btn-success next"><i class="fas fa-angle-double-right"></i></button>
        </div>
    </div>
</div>

<?php $this->view('template/bs4js'); ?>
<script>
    $('#namaSupplyer').on('keypress', function(e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            $('#telepon').focus();
        }
    })
    $('#telepon').on('keypress', function(e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            $.post('<?= BASEURL; ?>Supplyer/simpan', {
                fmod: $("input[name='fmod']").val(),
                idSupplyer: $('#idSupplyer').val(),
                namaSupplyer: $('#namaSupplyer').val(),
                telepon: $('#telepon').val(),
            }, function(res) {
                console.log(res);
                if (res == '1') {
                    $('.alert').addClass('text-success');
                    $('.alert').text('Data berhasil disimpan');
                    setTimeout(function() {
                        $('.alert').text('')
                    }, 2000);
                    emptyForm();
                    loadSupplyer();
                    $("input[name='fmod']").val('new');
                } else {
                    $('.alert').addClass('text-danger');
                    $('.alert').text('Data berhasil disimpan');
                    setTimeout(function() {
                        $('.alert').text('')
                    }, 2000);
                }
            })
        }
    })

    $('#dftSupplyer').on('click', '.chg', function() {
        let idSupplyer = $(this).parent('td').parent('tr').children('td:nth-child(1)').children('span').text();
        let namaSupplyer = $(this).parent('td').parent('tr').children('td:nth-child(2)').text();
        let telepon = $(this).parent('td').parent('tr').children('td:nth-child(3)').text();
        $('#idSupplyer').val(idSupplyer);
        $('#namaSupplyer').val(namaSupplyer);
        $('#telepon').val(telepon);
        $("input[name='fmod']").val('chg');
    })
    $('#dftSupplyer').on('click', '.del', function() {
        let idSupplyer = $(this).parent('td').parent('tr').children('td:nth-child(1)').children('span').text();
        let tenan = confirm('Supplyer akan dihapus');
        if (tenan == true) {
            $.post('<?= BASEURL; ?>Supplyer/afkir', {
                idSupplyer: idSupplyer
            }, function(res) {
                if (res == "1") {
                    loadSupplyer();
                    $('.alert').text('Data berhasil dihapus');
                    setTimeout(function() {
                        $('.alert').text('')
                    }, 2000);
                    emptyForm();
                    loadSupplyer();
                }
            })
        }
    })
    var pn = <?= $data['pn']; ?>;
    $('.prev').click(function() {
        let np = pn - 1;
        if (pn == 1) {
            window.location.href = "<?= BASEURL; ?>Supplyer/";
        } else {
            window.location.href = "<?= BASEURL; ?>Supplyer/" + np;
        }
    })

    $('.next').click(function() {
        let np = pn + 1;
        window.location.href = "<?= BASEURL; ?>Supplyer/" + np;
    })

    $('#katakunci').keypress(function(e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            let keyword = this.value,
                namaSupplyer = keyword.replace(/\ /g, "-"),
                nomor = 1;

            $.getJSON(
                '<?= BASEURL; ?>Supplyer/cari/' + namaSupplyer,
                function(res) {
                    $('#dftSupplyer tr').remove();
                    $.each(res, function(i, data) {
                        $('#dftSupplyer').append(`
                        <tr>
                        <td>${nomor++}<span class='ngumpet'>${data.idSupplyer}</span></td>
                        <td>${data.namaSupplyer}</td>
                        <td>${data.telepon}</td>
                        <td>
                            <a href="javascript:void(0)" class="chg"><i class="fas fa-edit"></i></a>
                            <a href="javascript:void(0)" class="del"><i class="fas fa-times text-danger"></i></a>
                        </td>
                        </tr>
                    `)
                    })
                }
            );
        }
    })

    function emptyForm() {
        $('#idSupplyer').val('');
        $('#namaSupplyer').val('');
        $('#namaSupplyer').focus();
        $('#telepon').val('');
    }

    function loadSupplyer() {
        $.getJSON(
            "<?= BASEURL; ?>Supplyer/loSupplyer/<?= $data['pn']; ?>",
            function(res) {
                $('#dftSupplyer tr').remove();
                let nomor = <?= $basenomor; ?>;
                $.each(res, function(i, data) {
                    $('#dftSupplyer').append(`
                    <tr>
                        <td>${nomor++}<span class='ngumpet'>${data.idSupplyer}</span></td>
                        <td>${data.namaSupplyer}</td>
                        <td>${data.telepon}</td>
                        <td>
                            <a href="javascript:void(0)" class="chg"><i class="fas fa-edit"></i></a>
                            <a href="javascript:void(0)" class="del"><i class="fas fa-times text-danger"></i></a>
                        </td>
                    </tr>
                    `)
                })
            }
        );
    }
</script>