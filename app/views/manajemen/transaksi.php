<div class="row row-top">
    <div class="col">
        <h4>Riwayat Transaksi</h4>
    </div>
</div>
<div class="row">
    <div class="col-lg-4">
        <table class="table table-sm">
            <tbody>
                <tr>
                    <td>ID Client</td>
                    <td><?= $data['client']['idClient']; ?></td>
                </tr>
                <tr>
                    <td>Nama Client</td>
                    <td><?= $data['client']['namaClient']; ?> [<?= $data['client']['tipeClient']; ?>] </td>
                </tr>
                <tr>
                    <td>Nomor Telepon</td>
                    <td><?= $data['client']['telepon']; ?></td>
                </tr>
                <tr>
                    <td colspan="2" class="text-right pr-2"><a href="<?= BASEURL; ?>Clients">Kembali</a></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="row">
    <div class="col table-responsive">
        <table class="table table-sm table-striped">
            <thead class="tableHeader">
                <tr>
                    <th>ID Invoice</th>
                    <th>Tanggal</th>
                    <th>Nama Barang</th>
                    <th>Quantity</th>
                    <th>Harga</th>
                    <th>Jumlah</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($data['trxes'] as $trx) : ?>
                    <tr>
                        <td> <a href="#" class="invoice"> <?= $trx['idInvoice']; ?> </a> </td>
                        <td><?= $trx['tanggal']; ?></td>
                        <td><?= $trx['namaBarang']; ?></td>
                        <td><?= $trx['quantity']; ?></td>
                        <td class="text-right pr-3"><?= number_format($trx['hargaJual'], 0, ',', '.'); ?></td>
                        <td class="text-right pr-3"><?= number_format($trx['jumlahHarga'], 0, ',', '.'); ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
<!-- modals -->
<!-- modal Invoice -->
<div class="modal" tabindex="-1" id="modalInvoice">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Invoice #<div id="idInvoice"></div>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="invoiceData">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>

<?php $this->view('template/bs4js'); ?>
<script>
    $('.invoice').click(function() {
        let id = $(this).text();
        $('#invoiceData').html('');
        $.ajax({
            url: "<?= BASEURL . "Clients/myInvoice/"; ?>" + id,
            success: function(res) {
                $('#invoiceData').html(res);
            }
        })
        $('#modalInvoice').modal('show');
    })

    function waybillCopy() {
        /* Get the text field */
        var copyText = document.getElementById("myInput");

        /* Select the text field */
        copyText.select();
        copyText.setSelectionRange(0, 99999); /* For mobile devices */

        /* Copy the text inside the text field */
        document.execCommand("copy");

        /* Alert the copied text */
        alert("Copied the text: " + copyText.value);
    }
</script>