<div class="row row-top">
    <div class="col">
        <h4>Riwayat Transaksi</h4>
    </div>
</div>
<div class="row">
    <div class="col-lg-4">
        <table class="table table-sm">
            <tbody>
                <tr>
                    <td>ID Barang</td>
                    <td><?= $data['barang']['idBarang']; ?></td>
                </tr>
                <tr>
                    <td>Nama Client</td>
                    <td><?= $data['barang']['namaBarang']; ?></td>
                </tr>

                <tr>
                    <td colspan="2" class="text-right pr-2"><a href="<?= BASEURL; ?>Barang">Kembali</a></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="row">
    <div class="col table-responsive">
        <table class="table table-sm table-striped">
            <thead class="tableHeader">
                <tr>
                    <th>ID Invoice</th>
                    <th>Tanggal</th>
                    <th>Nama Pembeli</th>
                    <th>Quantity</th>
                    <th>Harga Beli</th>
                    <th>Harga Jual</th>
                    <th>Jumlah</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($data['items'] as $item) : ?>
                    <tr>
                        <td><?= $item['idInvoice']; ?></td>
                        <td><?= $item['tanggal']; ?></td>
                        <td><?= $item['namaClient']; ?></td>
                        <td><?= $item['quantity']; ?></td>
                        <td class="text-right pr-3"><?= number_format($item['hargaBeli'], 0, ',', '.'); ?></td>
                        <td class="text-right pr-3"><?= number_format($item['hargaJual'], 0, ',', '.'); ?></td>
                        <td class="text-right pr-3"><?= number_format($item['jumlahHarga'], 0, ',', '.'); ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>