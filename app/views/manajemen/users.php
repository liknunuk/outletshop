<?php
$posisi = [
    'frontOffice' => 'Sales Service',
    'management' => 'Manajemen',
    'supervisor' => 'Supervisor'
];

$nomor = ($data['pn'] - 1) * rows + 1;
?>
<div class="row row-top">
    <div class="col">
        <h4>PENGGUNA SISTEM</h4>
    </div>
</div>
<div class="row">
    <div class="col-lg-3">
        <form action="<?= BASEURL; ?>Users/setUser" method="post">
            <div class="form-group">
                <label for="namaLengkap" class="mb-0">Nama Lengkap</label>
                <input autofocus type="text" name="namaLengkap" id="namaLengkap" class="form-control">
            </div>

            <div class="form-group">
                <label for="userGroup" class="mb-0">Hak Akses</label>
                <select name="userGroup" id="userGroup" class="form-control">
                    <?php foreach ($posisi as $val => $lab) : ?>
                        <option value="<?= $val; ?>"><?= $lab; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>

            <div class="form-group">
                <label for="userName" class="mb-0">Username</label>
                <input type="text" name="userName" id="userName" class="form-control">
            </div>

            <div class="form-group">
                <label for="authKey" class="mb-0">Password</label>
                <input type="password" name="authKey" id="authKey" class="form-control">
            </div>

            <input type="hidden" name="idUser" id="idUser" value="0">
            <input type="hidden" name="fmod" id="fmod" value="new">
            <div class="d-flex justify-content-center">
                <input type="submit" value="Simpan" class="btn btn-primary" id="addUser">
            </div>
        </form>
    </div>
    <div class="col-lg-9">
        <?php Alert::sankil(); ?>
        <div class="table-responsive">
            <table class="table table-sm table-striped">
                <thead class="tableHeader">
                    <tr>
                        <th>No.</th>
                        <th>Nama Lengkap</th>
                        <th>Hak Akses</th>
                        <th>Username</th>
                        <th><i class="fas fa-gear"></i></th>
                    </tr>
                </thead>
                <tbody id="dftUsers"></tbody>
            </table>
        </div>
    </div>
</div>
<?php $this->view('template/bs4js'); ?>
<script>
    var BASEURL = "<?= BASEURL; ?>";
    $(document).ready(function() {
        loadUsers("<?= $data['pn']; ?>");
    })

    $('#dftUsers').on('click', '.chg', function() {
        let idUser, namaLengkap, userGroup;
        idUser = $(this).parent('td').parent('tr').children('td:nth-child(1)').children('span').text();
        namaLengkap = $(this).parent('td').parent('tr').children('td:nth-child(2)').text();
        userGroup = $(this).parent('td').parent('tr').children('td:nth-child(3)').children('span').text();

        $('#namaLengkap').val(namaLengkap);
        $('#idUser').val(idUser);
        $('#userGroup option[value="' + userGroup + '"]').prop('selected', true);
        $('#fmod').val('chg');
        $('#authKey').prop('disabled', true);
        $('#userName').prop('disabled', true);
    })

    $('#dftUsers').on('click', '.pwd', function() {
        let idUser = $(this).parent('td').parent('tr').children('td:nth-child(1)').children('span').text();
        let userName = $(this).parent('td').parent('tr').children('td:nth-child(4)').text();
        $('#fmod').val('pwd');
        $('#userName').val(userName);
        $('#userName').prop('readonly', true);
        $('#authKey').val('');
        $('#authKey').focus();
        $('#idUser').val(idUser);
    })

    $('#dftUsers').on('click', '.del', function() {
        let tenan = confirm('User akan dihapus?');
        let idUser = $(this).parent('td').parent('tr').children('td:nth-child(1)').children('span').text();
        if (tenan == true) {
            $.post(BASEURL + "Users/setUser", {
                fmod: 'rmv',
                idUser: idUser
            }, function(res) {
                if (res == "1") {
                    loadUsers("<?= $data['pn']; ?>");
                }
            })
        }
    })


    function loadUsers(pn) {
        $.getJSON(BASEURL + 'Users/loUsers', function(res) {
            $("#dftUsers tr").remove();
            let nomor = parseInt(<?= $nomor; ?>);
            $.each(res, function(i, data) {
                let hakAkses = posisi(data.userGroup);
                $('#dftUsers').append(`
                <tr>
                <td class='text-center'>
                ${nomor++}. <span class='ngumpet'>${data.idUser}</span>
                </td>
                <td>${data.namaLengkap}</td>
                <td>${hakAkses}<span class="ngumpet">${data.userGroup}</span></td>
                <td>${data.userName}</td>
                <td>
                    <a href="javascript:void(0)" class="chg"><i class='fas fa-edit'></i></a>
                    <a href="javascript:void(0)" class="pwd"><i class='fas fa-lock'></i></a>
                    <a href="javascript:void(0)" class="del"><i class='fas fa-times'></i></a>
                </td>
                </tr>
                `)
            })

            $('input').prop('disabled', false);
            $('input').prop('readonly', false);
        })
    }



    function posisi(pos) {
        var posisi = {
            'frontOffice': 'Sales Service',
            'management': 'Manajemen',
            'supervisor': 'Supervisor'
        };

        return posisi[pos];
    }
</script>