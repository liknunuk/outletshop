<?php
$link = [];
foreach ($data['expeditor'] as $expeditor) {
    $nama = $expeditor['nama'];
    $url = $expeditor['track'];
    $link[$nama] = $url;
}
?>
<div class="row row-top">
    <div class="col">
        <h4>DATA PENGIRIMAN</h4>
    </div>
</div>
<div class="row">
    <div class="col">
        <?php Alert::sankil(); ?>
        <div class="table-responsive">
            <table class="table table-sm table-striped">
                <thead>
                    <tr>
                        <th>No. Invoice</th>
                        <th>Tanggal</th>
                        <th>Pelanggan</th>
                        <th>Ekpedisi</th>
                        <th>Nomor Resi</th>
                        <th><i class="fas fa-tasks"></i></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($data['invoices'] as $invoice) : ?>
                        <tr>
                            <td><?= $invoice['idInvoice']; ?></td>
                            <td><?= $invoice['tanggal']; ?></td>
                            <td><?= $invoice['namaClient']; ?></td>
                            <td><?= $invoice['expedisi']; ?></td>
                            <td id="awb_<?= $invoice['waybill']; ?>"><?= $invoice['waybill']; ?></td>
                            <td>
                                <a href="#" class="setExpedisi">
                                    <i class="fas fa-shuttle-van text-primary"></i>
                                </a>
                                <?php $exp = $invoice['expedisi']; ?>
                                <button type="button" data-clipboard-target="#awb_<?= $invoice['waybill']; ?>" class="btn btn-primary btn-sm clipb track" id="<?= $link[$exp]; ?>">Track</button>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- modal -->
<!-- modalExpedisi -->
<div class="modal" tabindex="-1" id="modalExpedisi">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Pengiriman Barang</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?= BASEURL; ?>Ekspedisi/setEkspedisi" method="post">
                <div class="modal-body">
                    <div class="form-group row">
                        <label for="expedisi" class="col-sm-6">Jasa Ekpedisi</label>
                        <div class="col-sm-6">
                            <select name="expedisi" id="expedisi" class="form-control">
                                <option value="">Pilih Salah Satu</option>
                                <?php $i = 1;
                                foreach ($data['expeditor'] as $expdt) : ?>
                                    <option value="<?= $expdt['nama']; ?>" <?php echo $i == 1 ? "Selected" : ""; ?>><?= $expdt['nama']; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="waybill" class="col-sm-6">No. Resi Pengiriman</label>
                        <div class="col-sm-6">
                            <input type="text" name="waybill" id="waybill" class="form-control">
                        </div>
                    </div>
                    <input type="hidden" name="idInvoice" id="idInvoice" value="">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>

<?php $this->view('template/bs4js'); ?>
<script src="<?= BASEURL . 'js/clipboard.min.js'; ?>"></script>
<script>
    new ClipboardJS('.clipb');
    $('.setExpedisi').click(function() {
        let idInvoice = $(this).parent('td').parent('tr').children('td:nth-child(1)').text(),
            expedisi = $(this).parent('td').parent('tr').children('td:nth-child(4)').text(),
            waybill = $(this).parent('td').parent('tr').children('td:nth-child(5)').text();

        $('#idInvoice').val(idInvoice);
        $('#expedisi').val(expedisi);
        $('#waybill').val(waybill);

        $('#modalExpedisi').modal('show');
    })

    $('.track').click(function() {
        let url = this.id;
        setInterval(redir(url), 2000);
        location.reload();
    })

    function redir(url) {
        window.open(url, '_blank');
    }
</script>