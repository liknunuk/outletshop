<div class="row row-top">
    <div class="col-lg-8 mx-auto">
        <h4>Barang Terlaris<br /><small>Bulan : <?= $data['bulan']; ?></small></h4>
    </div>
</div>
<div class="row">
    <div class="col-lg-8 mx-auto mt-3">
        <div class="form-inline">
            <label for="Bulan" class="mr-5">Pilih Bulan</label>
            <select id="Bulan" class="form-control" value="<?= $data['bulan']; ?>">
                <?php
                $tahunKini = date('Y');
                $tahunLalu = $tahunKini - 1;
                $bulanKini = date('m');
                $enamBulan = $bulanKini - 6;
                for ($b = $bulanKini; $b >= $enamBulan; $b--) : ?>
                    <?php if ($b < 1) :
                        $bulan = $tahunLalu . '-' . sprintf("%02d", (12 + $b));
                    ?>

                        <option <?php echo $bulan == $data['bulan'] ? "selected" : ""; ?> value="<?= $bulan; ?>"><?= $bulan; ?></option>
                    <?php else :
                        $bulan = $tahunKini . '-' . sprintf('%02d', $b);
                    ?>
                        <option <?php echo $bulan == $data['bulan'] ? "selected" : ""; ?> value="<?= $bulan; ?>"><?= $bulan; ?></option>
                    <?php endif; ?>
                <?php endfor; ?>
            </select>

        </div>
        <table class="table table-sm table-bordered">
            <thead class="tableHeader">
                <tr>
                    <th>Nomor</th>
                    <th>Nama Barang</th>
                    <th>Terjual</th>
                </tr>
            </thead>
            <tbody class="tbodylaporan">
                <?php
                $nomor = 1;
                foreach ($data['sale'] as $sale) : ?>
                    <tr>
                        <td class="text-center"><?= $nomor++; ?>.</td>
                        <td><?= $sale['namaBarang']; ?></td>
                        <td class="text-right"><?= $sale['quantity']; ?></td>
                    </tr>
                <?php endforeach; ?>

            </tbody>
        </table>
    </div>
</div>
<?php $this->view('template/bs4js'); ?>
<script>
    $('#Bulan').change(function() {
        window.location.href = "<?= BASEURL; ?>Sales/topSale/" + this.value;
    })
</script>