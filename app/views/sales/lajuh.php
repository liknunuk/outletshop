<?php
function tanggal($tanggal)
{
    list($t, $b, $h) = explode("-", $tanggal);
    return "$h/$b/$t";
}
?>
<div class="row row-top">
    <div class="col-lg-8 mx-auto">
        <?php if ($data['tanggal'] == $data['sampai']) : ?>
            <h4>Laporan Penjualan Harian<br />
                Tipe Klien: <?= $data['klien']; ?><br>
                <small>Tanggal : <?= tanggal($data['tanggal']); ?></small>
            </h4>
        <?php else : ?>
            <h4>Laporan Penjualan Harian<br /><small>Tanggal : <?= tanggal($data['tanggal']); ?> s.d <?= tanggal($data['sampai']); ?></small></h4>
        <?php endif; ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-10 mx-auto mt-3">
        <div class="form-inline">
            <label for="tanggal" class="mr-5">Pilih Tanggal</label>
            <input type="date" id="tanggal" class="form-control" value="<?= $data['tanggal']; ?>">
            <span class="mx-3">Sampai</span>
            <input type="date" id="sampai" class="form-control" value="<?= $data['sampai']; ?>">
            <i class="far fa-folder-open ml-3 btn" id="openReport" style="font-size:24px;"></i>
            <select id="klien" class="form-control">
                <option value="">Pilih Tipe Klien</option>
                <option value="Member">Member</option>
                <option value="Reseller">Reseller</option>
                <option value="User">User</option>
                <option value="semua">Semua</option>
            </select>
        </div>
        <table class="table table-sm table-bordered mt-3">
            <thead class="tableHeader">
                <tr>
                    <th>No. Invoice</th>
                    <th>Klien</th>
                    <th>Barang</th>
                    <th>Banyaknya</th>
                    <th>Pending</th>
                    <th>hargaBeli</th>
                    <th>Harga Jual</th>
                    <th>Jumlah</th>
                    <th>Laba Penjualan</th>
                    <th><i class="fas fa-gear"></i></th>
                </tr>
            </thead>
            <tbody class="tbodylaporan">
                <?php
                $grandTotal = 0;
                $profit = 0;
                foreach ($data['penjualan'] as $jual) : ?>
                    <tr>
                        <td>
                            <?= $jual['idInvoice']; ?>
                        </td>
                        <td><?= $jual['tipeClient']; ?></td>
                        <td><?= $jual['namaBarang']; ?></td>
                        <td class="text-right"><?= $jual['quantity']; ?></td>
                        <td class="text-right"><?= $jual['pending']; ?></td>
                        <td class="text-right"><?= number_format($jual['hargaBeli'], 0, ',', '.'); ?></td>
                        <td class="text-right"><?= number_format($jual['hargaJual'], 0, ',', '.'); ?></td>
                        <td class="text-right"><?= number_format($jual['subtotal'], 0, ',', '.'); ?></td>
                        <td class="text-right"><?= number_format($jual['laba'], 0, ',', '.'); ?></td>
                        <td>
                            <?php if ($jual['pending'] > 0) : ?>
                                <a href="javascript:void(0)" class="reset" id="<?= $jual['idInvoice'] . '_' . $jual['idBarang'] . '_' . $jual['pending']; ?>">Reset</a>
                            <?php else : ?>
                                Clear
                            <?php endif; ?>
                            &nbsp;
                            <a href="javascript:void(0)" class="delInv"><i class="fas fa-trash text-danger"></i></a>
                        </td>
                    </tr>
                <?php
                    $grandTotal += $jual['subtotal'];
                    $profit += $jual['laba'];
                endforeach; ?>
                <tr>
                    <td class="text-right" colspan="7">Jumlah Keseluruhan</td>
                    <td class="text-right"><?= number_format($grandTotal, 0, ',', '.'); ?></td>
                    <td class="text-right"><?= number_format($profit, 0, ',', '.'); ?></td>
                    <td>&nbsp;</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<?php $this->view('template/bs4js'); ?>
<script>
    $('#tanggal').change(function() {
        $('#sampai').val(this.value);
    })

    $('#openReport').click(function() {
        let from = $('#tanggal').val();
        let till = $('#sampai').val();
        window.location.href = "<?= BASEURL; ?>Sales/laju/<?= $data['klien']; ?>/" + from + "/" + till;
    })

    $(".reset").click(function() {
        let par = this.id;
        $.post('<?= BASEURL; ?>Sales/depending', {
            par: par
        }, function(res) {
            if (res == "1") {

                location.reload();
            }
        })
    })

    $('#klien').change(function() {
        let tipe = this.value;
        let from = $('#tanggal').val();
        let till = $('#sampai').val();
        window.location.href = "<?= BASEURL; ?>Sales/laju/" + tipe + "/" + from + "/" + till;
    })

    $(".delInv").click(function() {
        let id = $(this).parent('td').parent('tr').children('td:nth-child(1)').text();
        id = id.trim();
        let tenan = confirm('Data invoice Nomor: ' + id + ' akan dihapus!');
        if (tenan == true) {
            $.post('<?= BASEURL; ?>Sales/cancelInvoice', {
                idInvoice: id
            }, function(res) {
                console.log(res);
                if (res == "1") {
                    alert('Data berhsil dihapus');
                    location.reload();
                }
            })
        }

    })
</script>