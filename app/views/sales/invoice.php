<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Invoice: <?= $data['invoice']['idInvoice']; ?></title>
    <style>
        html,
        body {
            font-family: monospace;
            font-size: 10pt;
        }

        .corpid>h4 {
            font-size: 24px;
            margin: 0;
        }

        .corpid>p,
        .corpid>small {
            margin: 0;
        }

        .invodata label {
            display: inline-block;
            width: 100px;
        }

        .harga {
            text-align: right;
            padding-right: 5px;
        }

        img#logo {
            width: 40px;
            height: 40px;
        }

        .medsos>tbody>tr>td {
            text-align: left;
        }

        .medsos>tbody>tr>td>img {
            width: 20px;
        }
    </style>
</head>

<body onload=window.print()>
    <table style="margin-bottom:25px;">
        <tbody>
            <tr>
                <td align="center">
                    <img src="<?= BASEURL; ?>img/logob.png" alt="msglow_banjarnegara" id="logo">
                </td>
                <td class='corpid'>
                    <h4>MSGLOW BANJARNEGARA</h4>
                    <p>Jln. Letjend Soeprapto No. 34 A<br />Wangon Banjarnegara</p>
                </td>
            </tr>
        </tbody>
    </table>
    <div class="invodata">
        <label>No. Invoice</label>
        <span>:&nbsp;<?= $data['invoice']['idInvoice']; ?> / <?= $data['invoice']['tgInvoice']; ?></span>
    </div>
    <div class="invodata">
        <label>Untuk</label>
        <span>:&nbsp;<?= $data['invoice']['namaClient']; ?> (<?= $data['invoice']['tipeClient']; ?>) </span>
    </div>
    <hr>
    <table class="medsos">
        <tbody>
            <tr>
                <td><img src="<?= BASEURL; ?>img/wa-black.png" alt="whatsapp"></td>
                <td>&plus;6285642938486</td>
            </tr>
            <tr>
                <td><img src="<?= BASEURL; ?>img/ig-black.png" alt="instagram"></td>
                <td>@msglowbanjarnegara</td>
            </tr>
            <tr>
                <td><img src="<?= BASEURL; ?>img/shopee-bw.png" alt="shopee"></td>
                <td>https://shopee.co.id/msglowbanjarnegara_</td>
            </tr>
        </tbody>
    </table>
    <br>
    <table width="100%">
        <tbody>
            <?php foreach ($data['barang'] as $barang) : ?>
                <tr>
                    <td><?= $barang['quantity']; ?> x <?= $barang['namaBarang']; ?></td>
                    <td class="harga"><?= number_format($barang['hargaJual'], 0, ',', '.'); ?></td>
                    <td class="harga"><?= number_format(($barang['hargaJual'] * $barang['quantity']), 0, ',', '.'); ?></td>
                </tr>
            <?php endforeach; ?>
            <tr>
                <td class='text-center' colspan='2'>Ongkos Kirim</td>
                <td class="harga"><?= number_format($data['invoice']['ongkir']); ?></td>
            </tr>
            <tr>
                <td class='text-center' colspan='2'>Jumlah Total</td>
                <td class="harga"><?= number_format($data['invoice']['grandTotal']); ?></td>
            </tr>
            <tr>
                <td class='text-center' colspan='2'>Dibayarkan</td>
                <td class="harga"><?= number_format($data['invoice']['bayar']); ?></td>
            </tr>
            <tr>
                <td class='text-center' colspan='2'><a href='<?= BASEURL; ?>Sales'>Kembali</a></td>
                <td class="harga"><?= number_format($data['invoice']['kembali']); ?></td>
            </tr>
        </tbody>
    </table>
    <br><br>
    <div style="text-align:center; font-size: 10px;">
        <span>Terima kasih atas kepercayaan Anda pada MSGLOW Banjarnegara</span><br>
        <span style="margin-top:10px"> <strong>Note: Barang yang sudah dibeli tidak dapat ditukarkan kembali</strong></span>
    </div>

</body>

</html>