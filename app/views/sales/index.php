<div class="row row-top">
    <div class="col">
        <h4>PENJUALAN</h4>
    </div>
</div>
<div class="row">
    <div class="col-md-3">
        <div>

            <label class="mb-0" for="tanggal"><small class="smallfont">Tanggal</small></label>
            <input type="date" id="tanggal" class="form-control form-control-sm" value="<?= date('Y-m-d'); ?>">

            <label class="mb-0" for="idClient"><small class="smallfont">Pembeli</small></label>
            <input type="text" id="idClient" class="form-control form-control-sm" value="User-User" list="loClients">
            <datalist id="loClients"></datalist>

            <button class="btn btn-primary btn-sm form-control mt-2" id="invoiceBaru">Tambah Penjualan</button>
        </div>

        <h4 class="mt-3">Buka Invoice</h4>
        <input type="text" class="form-control" id="reqIdInvoice" placeholder="Nomor Invoice">
        <button class="btn btn-primary btn-sm mt-1 float-right" id="cariInvoice">Buka Data</button>
        <!--
        <div>
            <div class="form-group mb-1">
                <label for="tanggal" class="mb-0"><small class="smallfont">Tanggal</small></label>
                <input type="date" id="citanggal" class="form-control form-control-sm">
            </div>
            <div class="form-group mb-1">
                <label for="klien" class="mb-0"><small class="smallfont">Nama / ID Klien</small></label>
                <input type="text" id="ciklien" class="form-control form-control-sm">
            </div>
            <div class="form-group mt-3">
                <button class="btn btn-success" id="cariInvoice">Cari Invoice</button>
            </div>
        </div>
        

        <div class="list-group" id="loInvoices"></div>
        -->
    </div>
    <div class="col-md-9">
        <div class="row">
            <div class="col-md-6">
                <table class="table table-sm">
                    <tbody>
                        <tr>
                            <td>Nomor Invoice</td>
                            <td id="trxInvoice"></td>
                        </tr>
                        <tr>
                            <td>Tanggal</td>
                            <td id="trxTanggal"></td>
                        </tr>
                        <tr>
                            <td>ID Pembeli</td>
                            <td><span id="trxClient"></span> - <span id="tipeClient"></span> </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-md-6">
                <div class="form-horizontal">

                    <div class="form-group mb-0 row">
                        <label for="trxBarang" class="col-sm-4">Kode Barang</label>
                        <div class="col-sm-8">
                            <input type="text" id="trxBarang" class="form-control" placeholder="Tulis Kode Barang" list="loBarang">
                            <datalist id="loBarang"></datalist>
                        </div>
                    </div>

                    <div class="form-group mb-0 row">
                        <label for="trxQuantity" class="col-sm-4">Banyaknya</label>
                        <div class="col-sm-8">
                            <input type="number" id="trxQuantity" class="form-control text-right">
                        </div>
                    </div>

                    <div class="form-group mb-0 row">
                        <label for="trxHargaJual" class="col-sm-4">Harga</label>
                        <div class="col-sm-8">
                            <input type="number" id="trxHargaJual" class="form-control text-right">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mb-3">
            <div class="col table-responsive">
                <table class="table table-bordered table-sm">
                    <thead>
                        <tr class="tableHeader">
                            <th>No.</th>
                            <th>Nama Barang</th>
                            <th>Banyaknya</th>
                            <th>Harga</th>
                            <th>Jumlah</th>
                            <th><i class="fas fa-tasks"></i></th>
                        </tr>
                    </thead>
                    <tbody id="dataInvoice"></tbody>
                    <tbody>
                        <tr>
                            <td colspan='4' class='text-right'>Jumlah Keseluruhan</td>
                            <td class="text-right" id="trxTotal"></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan='4' class='text-right'>Ongkos Kirim</td>
                            <td class="text-right"><input type="number" id="trxOngkir" class="form-control text-right"></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan='4' class='text-right'>Total Biaya</td>
                            <td class="text-right" id="trxGrtot"></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan='4' class='text-right'>Bayar</td>
                            <td class='text-right'><input type="number" id="trxBayar" class="form-control text-right" value="0"></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan='4' class='text-right'>Kembali</td>
                            <td id="trxJujul" class='text-right'>0</td>
                            <td>&nbsp;</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- Modals -->
<!-- ModalPending -->
<div class="modal" tabindex="-1" id="modalPending">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Pending Item</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <div class="form-group row">
                    <label for="pending" class="col-sm-6">Jumlah</label>
                    <div class="col-sm-6">
                        <input type="number" name="pending" id="pending" class="form-control" min="0" value="0">
                    </div>
                    <input type="hidden" name="idxPenjualan" id="idxPenjualan" value="">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" id="submit" class="btn btn-primary">Simpan</button>
            </div>
        </div>
    </div>
</div>
<?php $this->view('template/bs4js'); ?>
<script>
    var trxTotal = 0;
    var trxJujul = 0;
    // cari pelanggan
    $('#idClient').focus(function() {
        $(this).val('');
    })

    $('#idClient').keyup(function() {
        let nama = this.value;
        if (nama.length >= 4) {
            $.getJSON(
                '<?= BASEURL; ?>Clients/cari/' + nama,
                function(res) {
                    $('#loClients option').remove();
                    $.each(res, function(i, data) {
                        $('#loClients').append(`<option>${data.idClient}-${data.namaClient}</option>`)
                    })
                }
            )
        }
    })



    // invoice baru
    $('#invoiceBaru').click(function() {
        let tanggal = $('#tanggal').val(),
            idClient = $('#idClient').val().split('-');
        $('#trxTanggal').text(tanggal);
        $('#trxClient').text(idClient[0]);
        // tipe client

        $.ajax({
            url: '<?= BASEURL; ?>Sales/tipeclient/' + idClient[0],
            success: function(res) {
                $('#tipeClient').text(res);
            }
        });

        $.post(
            '<?= BASEURL; ?>Sales/newInvoice', {
                idClient: idClient[0],
                tanggal: tanggal
            },
            function(res) {
                $('#trxInvoice').text(res);
                $('#trxBarang').focus();
            }
        )
    })

    // cariKodeBarng
    $('#trxBarang').keyup(function() {
        let nb = this.value;
        nb = nb.replace(/\ /g, "-");
        if (nb.length >= 3) {
            $.getJSON(
                '<?= BASEURL; ?>Sales/caderang/' + nb,
                function(res) {
                    $('#loBarang option').remove();
                    $.each(res, function(i, data) {
                        $('#loBarang').append(`<option value="${data.idBarang}">${data.namaBarang}</option>`)
                    })
                }
            )
        }
    })
    // cariHarga
    $('#trxBarang').change(function() {
        $.ajax({
            dataType: 'json',
            url: '<?= BASEURL; ?>Sales/hargaJual/' + this.value.replace(/\ /g, "-"),
            success: function(res) {
                console.log('res:', res);
                $('#trxHargaJual').val(res.hargaJual);
                $('#trxQuantity').val(res.realStok);
                $('#trxQuantity').prop('max', res.realStok);
            }
        })
    })

    $('#trxHargaJual').keypress(function(e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            $.post('<?= BASEURL; ?>Sales/setSales', {
                idInvoice: $('#trxInvoice').text(),
                idBarang: $('#trxBarang').val(),
                quantity: $('#trxQuantity').val(),
                hargaJual: this.value
            }, function(res) {
                console.log(res);
                if (res == "1") {
                    loadSales($('#trxInvoice').text());
                    $('#trxBarang').val('');
                    $('#trxBarang').focus();
                    $('#trxQuantity').val('');
                    $('#trxHargaJual').val('');
                }
            })
        }
    })

    $('#trxBayar').keypress(function(e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            let bayar = parseInt(this.value),
                jujul = bayar - trxTotal;
            $('#trxJujul').text(jujul.toLocaleString('id-ID'));
            trxJujul = jujul;

            $.post(
                '<?= BASEURL; ?>Sales/closing', {
                    grandTotal: trxTotal,
                    bayar: $('#trxBayar').val(),
                    kembali: trxJujul,
                    ongkir: $('#trxOngkir').val(),
                    idInvoice: $('#trxInvoice').text()
                },
                function(res) {
                    console.log('respon: ', res);
                    if (res == '1') {
                        window.location.href = '<?= BASEURL; ?>Sales/invoice/' + $('#trxInvoice').text();
                    }
                }
            )
        }
    })

    $('#trxBarang').keypress(function(e) {
        if (e.keyCode == 13 && this.value == '') {
            $('#trxOngkir').focus();
            $('#trxOngkir').val('');
        } else if (e.keyCode == 13 && this.value != '') {
            $('#trxQuantity').focus();
        }
    })

    $('#trxQuantity').keypress(function(e) {
        let mxqty = parseInt($(this).prop('max'));
        let qntty = parseInt(this.value);
        if (e.keyCode == 13) {
            // alert('max:' + mxqty + ' - ' + ' buy:' + qntty);
            if (qntty <= mxqty) {
                $('#trxHargaJual').focus();
            } else {
                $('#trxQuantity').focus();
                $('#trxQuantity').val(mxqty);
            }

        }

    })

    $('#trxOngkir').keypress(function(e) {
        if (e.keyCode == 13) {
            let total, ongkir;
            total = parseInt($('#trxTotal').text());
            ongkir = parseInt(this.value);
            trxTotal = (trxTotal + ongkir);
            $('#trxBayar').focus();
            $('#trxBayar').val('');
            $('#trxGrtot').text(trxTotal.toLocaleString('id-ID'));
        }
    })

    $('#dataInvoice').on('click', '.wurung', function() {
        let idxPenjualan = this.id;
        let tenan = confirm('Barang batal dibeli?');
        if (tenan == true) {
            $.post('<?= BASEURL; ?>Sales/cancelItem', {
                idxPenjualan: idxPenjualan
            }, function(res) {
                if (res == "1") {
                    loadSales();
                }
            })
        }
    })

    $('#dataInvoice').on('click', '.mundur', function() {
        let idxPenjualan = this.id;
        $('#idxPenjualan').val(idxPenjualan);
        $('#pending').focus();
        $('#modalPending').modal('show');

    })

    $('#submit').click(function() {
        $.post('<?= BASEURL; ?>Sales/pendingItem', {
            idxPenjualan: $('#idxPenjualan').val(),
            pending: $('#pending').val()
        }, function(res) {
            console.log('pending beli: ', res);
            if (res == "1") {
                loadSales($('#trxInvoice').text());
                $('#modalPending').modal('toggle');
            }
        })
    })
    /*
        $('#cariInvoice').click(function() {
            $.ajax({
                method: 'POST',
                dataType: 'json',
                url: '<?= BASEURL; ?>Sales/invoiceSeek',
                data: {
                    tanggal: $('#citanggal').val(),
                    klien: $('#ciklien').val()
                },
                success: function(res) {
                    console.log(res);
                    $('#loInvoices li').remove();
                    $.each(res, function(i, data) {
                        $('#loInvoices').append(`
                        <li class='list-group-item py-1'>
                        <a href="javascript:void(0)" class='resIdInvoice'>${data.idInvoice}</a> - ${data.tanggal}<br/>
                        ${data.namaClient}
                        </li>
                        `)
                    })
                }
            })
        })

        $('#loInvoices').on('click', '.resIdInvoice', function() {
            $('#trxInvoice').text($(this).text());
            loadSales($(this).text());
        })
    */

    $('#cariInvoice').click(function() {
        let idInvoice = $('#reqIdInvoice').val();
        loadSales(idInvoice);
    })

    function loadSales(idInvoice) {
        // invoice information
        $.getJSON(
            '<?= BASEURL; ?>Sales/invoiceInfo/' + idInvoice,
            function(res) {
                $("#trxInvoice").text(res.idInvoice);
                $("#trxTanggal").text(res.tgInvoice);
                $("#trxClient").text(res.idClient);
                $("#tipeClient").text(res.tipeClient);
            }
        );
        // invoice items data
        $.getJSON(
            '<?= BASEURL; ?>Sales/invoiceData/' + idInvoice,
            function(res) {
                $('#dataInvoice tr').remove();
                let nomor = 1;
                let harga = 0,
                    jumlah = 0,
                    total = 0;
                $.each(res, function(i, data) {
                    harga = parseInt(data.hargaJual).toLocaleString('id-ID');
                    jumlah = parseInt(data.hargaJual) * parseInt(data.quantity);
                    console.log('jumlah: ', jumlah);
                    total = total + jumlah;
                    trxTotal = total;
                    $('#dataInvoice').append(`
                    <tr>
                        <td class="text-center">${nomor++}.</td>
                        <td>${data.namaBarang}</td>
                        <td class="text-center">${data.quantity} - (${data.pending})</td>
                        <td class="text-right">${harga}</td>
                        <td class="text-right">${jumlah.toLocaleString('id-ID')}</td>
                        <td>
                            <a href='javascript:void(0)' class='wurung' id='${data.idxPenjualan}'> <i class='fas fa-times' style='color:maroon;'></i></a>
                            <a href='javascript:void(0)' class='mundur' id='${data.idxPenjualan}'> <i class='fas fa-stopwatch' style='color:maroon;'></i></a>
                        </td>
                    </tr>
                    `)

                    $('#trxTotal').text(trxTotal.toLocaleString('id-ID'));
                    console.log('total: ', trxTotal);
                })
            }
        )
    }
</script>