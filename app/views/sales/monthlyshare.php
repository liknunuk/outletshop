<div class="row row-top">
    <div class="col-lg-8 mx-auto">
        <h4>Laporan Kontribusi Klient<br /><small>Bulan : <?= $data['bulan']; ?></small></h4>
    </div>
</div>
<div class="row">
    <div class="col-lg-8 mx-auto mt-3">
        <div class="form-inline">
            <label for="Bulan" class="mr-5">Pilih Bulan</label>
            <select id="Bulan" class="form-control" value="<?= $data['bulan']; ?>">
                <?php
                $tahunKini = date('Y');
                $tahunLalu = $tahunKini - 1;
                $bulanKini = date('m');
                $enamBulan = $bulanKini - 6;
                for ($b = $bulanKini; $b >= $enamBulan; $b--) : ?>
                    <?php if ($b < 1) :
                        $bulan = $tahunLalu . '-' . sprintf("%02d", (12 + $b));
                    ?>

                        <option <?php echo $bulan == $data['bulan'] ? "selected" : ""; ?> value="<?= $bulan; ?>"><?= $bulan; ?></option>
                    <?php else :
                        $bulan = $tahunKini . '-' . sprintf('%02d', $b);
                    ?>
                        <option <?php echo $bulan == $data['bulan'] ? "selected" : ""; ?> value="<?= $bulan; ?>"><?= $bulan; ?></option>
                    <?php endif; ?>
                <?php endfor; ?>
            </select>
            <label for="kategori" class="ml-3">Kategori</label>
            <select id="kategori" class="form-control ml-3">
                <?php
                function isSelected($data, $label)
                {
                    return $data == $label ? "selected" : "";
                }
                ?>
                <option <?= isSelected($data['kategori'], 'User'); ?> value="User">User</option>
                <option <?= isSelected($data['kategori'], 'Member'); ?> value="Member">Member</option>
                <option <?= isSelected($data['kategori'], 'Reseller'); ?> value="Reseller">Reseller</option>
            </select>

        </div>
        <table class="table table-sm table-bordered">
            <thead class="tableHeader">
                <tr>
                    <th>ID Klien</th>
                    <th>Nama Klien</th>
                    <th>Kategori</th>
                    <th>Kontribusi</th>
                </tr>
            </thead>
            <tbody class="tbodylaporan">
                <?php foreach ($data['laba'] as $laba) : ?>
                    <tr>
                        <td><?= $laba['idClient']; ?></td>
                        <td><?= $laba['namaClient']; ?></td>
                        <td><?= $laba['tipeClient']; ?></td>
                        <td class="text-right"><?= number_format($laba['kontribusi'], 0, ',', '.'); ?></td>
                    </tr>
                <?php endforeach; ?>

            </tbody>
        </table>
    </div>
</div>
<?php $this->view('template/bs4js'); ?>
<script>
    $('#Bulan').change(function() {
        window.location.href = "<?= BASEURL; ?>Sales/kontribusi/" + this.value;
    })

    $('#kategori').change(function() {
        window.location.href = "<?= BASEURL; ?>Sales/kontribusi/" + this.value + '/' + $('#Bulan').val();
    })
</script>