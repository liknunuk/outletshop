<!-- 
    Form Barang 
    idBarang , namaBarang , realStok , hargaPokok
-->

<form action="" method="post">
    <input type="hidden" name="fmod" id="fmod" value="">

    <div class="form-group row">
        <label for="idBarang" class="col-sm-4">Kode Barang</label>
        <div class="col-sm-8">
            <input type="text" name="idBarang" id="idBarang" class="form-control" value="">
        </div>
    </div>

    <div class="form-group row">
        <label for="namaBarang" class="col-sm-4">Nama Barang</label>
        <div class="col-sm-8">
            <input type="text" name="namaBarang" id="namaBarang" class="form-control" value="">
        </div>
    </div>

    <div class="form-group row">
        <label for="hargaPokok" class="col-sm-4">Harga Pokok</label>
        <div class="col-sm-8">
            <input type="number" name="hargaPokok" id="hargaPokok" class="form-control" value="" min="0">
        </div>
    </div>

    <div class="form-group d-flex justify-content-end">
        <button type="submit" class="btn btn-success">Simpan</button>
    </div>

</form>

<!-- 
    Form Supplyer 
    Columns: idSupplyer , namaSupplyer , telepon
-->

<form action="" method="post">
    <input type="hidden" name="fmod" id="fmod" value="">

    <div class="form-group row">
        <label for="idSupplyer" class="col-sm-4">Nomor Supplyer</label>
        <div class="col-sm-8">
            <input type="text" name="idSupplyer" id="idSupplyer" class="form-control" value="">
        </div>
    </div>

    <div class="form-group row">
        <label for="namaSupplyer" class="col-sm-4">Nama Supplyer</label>
        <div class="col-sm-8">
            <input type="text" name="namaSupplyer" id="namaSupplyer" class="form-control" value="">
        </div>
    </div>

    <div class="form-group row">
        <label for="telepon" class="col-sm-4">Nomor Telepon</label>
        <div class="col-sm-8">
            <input type="number" name="telepon" id="telepon" class="form-control" value="" min="0">
        </div>
    </div>

    <div class="form-group d-flex justify-content-end">
        <button type="submit" class="btn btn-success">Simpan</button>
    </div>

</form>

<!-- 
    Form Client
    Columns: idClient , namaClient, telepon, tipeClient
 -->
<form action="" method="post">
    <input type="hidden" name="fmod" id="fmod" value="">

    <div class="form-group row">
        <label for="idClient" class="col-sm-4">Nomor Pelanggan</label>
        <div class="col-sm-8">
            <input type="text" name="idClient" id="idClient" class="form-control" value="">
        </div>
    </div>

    <div class="form-group row">
        <label for="namaClient" class="col-sm-4">Nama Pelanggan</label>
        <div class="col-sm-8">
            <input type="text" name="namaClient" id="namaClient" class="form-control" value="">
        </div>
    </div>

    <div class="form-group row">
        <label for="telepon" class="col-sm-4">Nomor Telepon</label>
        <div class="col-sm-8">
            <input type="number" name="telepon" id="telepon" class="form-control" value="" min="0">
        </div>
    </div>

    <div class="form-group row">
        <label for="tipeClient" class="col-sm-4">Nomor tipeClient</label>
        <div class="col-sm-8">
            <select name="tipeClient" id="tipeClient" class="form-control">
                <!-- 'User','Member','Reseller' -->
                <option value="User">User pengecer</option>
                <option value="Member">Member</option>
                <option value="Reseller">Reseller</option>
            </select>
        </div>
    </div>

    <div class="form-group d-flex justify-content-end">
        <button type="submit" class="btn btn-success">Simpan</button>
    </div>

</form>

<!-- 
    Form Pembelian
    Columns: idInvoice , tanggal, idSupplyer , idBarang , quantity , hargaBeli, idxItem
 -->
<form action="" method="post">
    <input type="hidden" name="fmod" id="fmod" value="">
    <input type="hidden" name="idxItem" id="idxItem" value="">

    <div class="form-group row">
        <label for="idInvoice" class="col-sm-4">Nomor Nota / Invoice</label>
        <div class="col-sm-8">
            <input type="text" name="idInvoice" id="idInvoice" class="form-control" value="">
        </div>
    </div>

    <div class="form-group row">
        <label for="idSupplyer" class="col-sm-4">Nama Supplyer</label>
        <div class="col-sm-8">
            <input type="text" name="idSupplyer" id="idSupplyer" class="form-control" value="" list="losupplyer">
            <datalist id="losupplyer"></datalist>
        </div>
    </div>

    <div class="form-group row">
        <label for="idBarang" class="col-sm-4">Kode Barang / Barcode</label>
        <div class="col-sm-8">
            <input type="text" name="idBarang" id="idBarang" class="form-control" value="">
        </div>
    </div>

    <div class="form-group row">
        <label for="quantity" class="col-sm-4">Banyaknya</label>
        <div class="col-sm-8">
            <input type="number" name="quantity" id="quantity" class="form-control" value="" min="0">
        </div>
    </div>

    <div class="form-group row">
        <label for="hargaBeli" class="col-sm-4">Harga Pembelian</label>
        <div class="col-sm-8">
            <input type="number" name="hargaBeli" id="hargaBeli" class="form-control" value="" min="0">
        </div>
    </div>

    <div class="form-group d-flex justify-content-end">
        <button type="submit" class="btn btn-success">Simpan</button>
    </div>

</form>

<!-- 
    FORM invoice
    Columns : idInvoice , tanggal, idClient , grandTotal , bayar , kembali
 -->
<form action="" method="post">
    <input type="hidden" name="fmod" id="fmod" value="">

    <div class="form-group row">
        <label for="idInvoice" class="col-sm-4">Nomor Nota / Invoice</label>
        <div class="col-sm-8">
            <input type="text" name="idInvoice" id="idInvoice" class="form-control" value="">
        </div>
    </div>

    <div class="form-group row">
        <label for="tanggal" class="col-sm-4">Tanggal Pembelian</label>
        <div class="col-sm-8">
            <input type="date" name="tanggal" id="tanggal" class="form-control" value="">
        </div>
    </div>

    <div class="form-group row">
        <label for="idClient" class="col-sm-4">Nomor Pelanggan</label>
        <div class="col-sm-8">
            <input type="text" name="idClient" id="idClient" class="form-control" value="" list="loclient">
            <datalist id="loclient"></datalist>
        </div>
    </div>

    <div class="form-group d-flex justify-content-end">
        <button type="submit" class="btn btn-success">Lanjut</button>
    </div>

</form>

<!-- 
    Form Penjualan / isi Invoice
    Columns idxPenjualan , idInvoice , idBarang , quantity , hargaJual
 -->
<form action="" method="post">
    <input type="hidden" name="fmod" id="fmod" value="">
    <input type="hidden" name="idInvoice" id="idInvoice" value="">

    <div class="form-group row">
        <label for="idBarang" class="col-sm-4">Kode Barang / Barcode</label>
        <div class="col-sm-8">
            <input type="text" name="idBarang" id="idBarang" class="form-control" value="">
        </div>
    </div>

    <div class="form-group row">
        <label for="quantity" class="col-sm-4">Banyaknya</label>
        <div class="col-sm-8">
            <input type="number" name="quantity" id="quantity" class="form-control" value="">
        </div>
    </div>

    <div class="form-group row">
        <label for="hargaJual" class="col-sm-4">Harga Jual</label>
        <div class="col-sm-8">
            <input type="text" name="hargaJual" id="hargaJual" class="form-control" value="">
        </div>
    </div>

    <div class="form-group d-flex justify-content-end">
        <button type="submit" class="btn btn-success">Lanjut</button>
    </div>

</form>