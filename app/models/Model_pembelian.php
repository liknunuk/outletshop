<?php
class Model_pembelian
{
    private $table = "msg_pembelian";
    // Columns: tanggal, idSupplyer , idBarang , quantity , hargaBeli , idInvoice, idxItem
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }


    // Basic CRUD OPERATION //

    // INSERTION
    public function tambah($data)
    {
        $sql = "INSERT INTO $this->table SET tanggal=:tanggal, idSupplyer=:idSupplyer , idBarang=:idBarang , quantity=:quantity , hargaBeli=:hargaBeli , idInvoice=:idInvoice , idxItem=:idxItem";
        $this->db->query($sql);
        $this->db->bind('tanggal', $data['tanggal']);
        $this->db->bind('idSupplyer', $data['idSupplyer']);
        $this->db->bind('idBarang', $data['idBarang']);
        $this->db->bind('quantity', $data['quantity']);
        $this->db->bind('hargaBeli', $data['hargaBeli']);
        $this->db->bind('idInvoice', $data['idInvoice']);
        $this->db->bind('idxItem', NULL);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // UPDATE
    public function ngubah($data)
    {
        $sql = "UPDATE $this->table SET tanggal=:tanggal, idSupplyer=:idSupplyer , idBarang=:idBarang , quantity=:quantity , hargaBeli=:hargaBeli, idInvoice=:idInvoice WHERE idxItem=:idxItem ";
        $this->db->query($sql);
        $this->db->bind('tanggal', $data['tanggal']);
        $this->db->bind('idSupplyer', $data['idSupplyer']);
        $this->db->bind('idBarang', $data['idBarang']);
        $this->db->bind('quantity', $data['quantity']);
        $this->db->bind('hargaBeli', $data['hargaBeli']);
        $this->db->bind('idInvoice', $data['idInvoice']);
        $this->db->bind('idxItem', $data['idxItem']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // DELETE
    public function sampah($data)
    {
        $sql = "DELETE FROM $this->table WHERE idxItem=:idxItem";
        $this->db->query($sql);
        $this->db->bind('idxItem', $data['idxItem']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function notaWurung(string $data)
    {
        $sql = "DELETE FROM $this->table WHERE idInvoice=:idInvoice";
        $this->db->query($sql);
        $this->db->bind('idInvoice', $data['idInvoice']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // DISPLAY MULTIPLE
    public function tampil($pn = 1)
    {
        $row = ($pn - 1) * rows;
        $sql = "SELECT msg_pembelian.* , DATE_FORMAT(msg_pembelian.tanggal,'%d-%m-%Y') tgbeli, msg_supplyer.namaSupplyer , msg_barang.namaBarang FROM msg_pembelian, msg_supplyer , msg_barang WHERE msg_pembelian.idBarang = msg_barang.idBarang && msg_pembelian.idSupplyer = msg_supplyer.idSupplyer ORDER BY idxItem DESC LIMIT $row ," . rows;
        $this->db->query($sql);
        return $this->db->resultSet();
    }

    public function beliDari($idSupplyer)
    {
        $sql = "SELECT msg_pembelian.* , DATE_FORMAT(msg_pembelian.tanggal,'%d-%m-%Y') tgbeli, msg_supplyer.namaSupplyer , msg_barang.namaBarang FROM msg_pembelian, msg_supplyer , msg_barang WHERE msg_pembelian.idBarang = msg_barang.idBarang && msg_pembelian.idSupplyer = msg_supplyer.idSupplyer && msg_pembelian.idSupplyer = :idSupplyer ORDER BY idxItem DESC LIMIT 300";
        $this->db->query($sql);
        $this->db->bind('idSupplyer', $idSupplyer);
        return $this->db->resultSet();
    }

    public function beliBarang($idBarang)
    {
        $sql = "SELECT msg_pembelian.* , DATE_FORMAT(msg_pembelian.tanggal,'%d-%m-%Y') tgbeli, msg_supplyer.namaSupplyer , msg_barang.namaBarang FROM msg_pembelian, msg_supplyer , msg_barang WHERE msg_pembelian.idBarang = msg_barang.idBarang && msg_pembelian.idSupplyer = msg_supplyer.idSupplyer && msg_pembelian.idBarang = :idBarang ORDER BY idxItem DESC LIMIT 300";
        $this->db->query($sql);
        $this->db->bind('idBarang', $idBarang);
        return $this->db->resultSet();
    }

    public function byInvoice($idInvoice)
    {
        $sql = "SELECT msg_pembelian.* , DATE_FORMAT(msg_pembelian.tanggal,'%d-%m-%Y') tgbeli, msg_supplyer.namaSupplyer , msg_barang.namaBarang FROM msg_pembelian, msg_supplyer , msg_barang WHERE msg_pembelian.idBarang = msg_barang.idBarang && msg_pembelian.idSupplyer = msg_supplyer.idSupplyer && msg_pembelian.idInvoice = :idInvoice ORDER BY idxItem DESC LIMIT 300";
        $this->db->query($sql);
        $this->db->bind('idInvoice', $idInvoice);
        return $this->db->resultSet();
    }

    // DISPLAY SINGULAR
    public function detail($key)
    {
        $sql = "SELECT * FROM $this->table WHERE idxItem=:key ";
        $this->db->query($sql);
        $this->db->bind('key', $key);
        return $this->db->resultOne();
    }

    public function detailInvoice($key)
    {
        $sql = "SELECT * FROM $this->table WHERE idInvoice=:key ";
        $this->db->query($sql);
        $this->db->bind('key', $key);
        return $this->db->resultSet();
    }

    // filter pembelian
    public function filterbeli($col, $val)
    {
        if ($col == 'idInvoice') {
            $data = $this->optInvoice($val);
        } elseif ($col == 'idSupplyer') {
            $data = $this->optSupplyer($val);
        } elseif ($col == 'idBarang') {
            $data = $this->optBarang($val);
        } else {
            return array(['opt' => 0, 'val' => 0]);
        }
        return $data;
    }

    private function optInvoice($val)
    {
        $sql = "SELECT idInvoice opt , idInvoice val FROM $this->table WHERE idInvoice LIKE :val GROUP BY idInvoice ORDER BY idInvoice DESC LIMIT 300";
        // echo $sql;
        $this->db->query($sql);
        $this->db->bind('val', "%{$val}%");
        return $this->db->resultSet();
    }

    private function optSupplyer($val)
    {
        $sql = "SELECT idSupplyer opt , namaSupplyer val FROM msg_supplyer WHERE namaSupplyer LIKE :val ORDER BY namaSupplyer LIMIT 300";
        $this->db->query($sql);
        $this->db->bind('val', "%{$val}%");
        return $this->db->resultSet();
    }

    private function optBarang($val)
    {
        $val = str_replace("-", " ", $val);
        $sql = "SELECT idBarang opt , namaBarang val FROM msg_barang WHERE namaBarang LIKE :val ORDER BY namaBarang LIMIT 300 ";
        $this->db->query($sql);
        $this->db->bind('val', "%{$val}%");
        return $this->db->resultSet();
    }

    // daftar pembelian menurut filter
    public function beliby($col, $val)
    {
        $val = str_replace("-", " ", $val);
        $sql = "SELECT msg_pembelian.* , DATE_FORMAT(msg_pembelian.tanggal,'%d-%m-%Y') tgbeli, msg_supplyer.namaSupplyer , msg_barang.namaBarang FROM msg_pembelian, msg_supplyer , msg_barang WHERE msg_pembelian.idBarang = msg_barang.idBarang && msg_pembelian.idSupplyer = msg_supplyer.idSupplyer && msg_pembelian.{$col}=:val ORDER BY idxItem DESC LIMIT 300";
        // echo $sql;
        $this->db->query($sql);
        $this->db->bind('val', $val);
        return $this->db->resultSet();
    }

    // CUSTOMIZED QUERY //
    public function something($data)
    {
        // $sql = "";
        // $this->db->query($sql);
        // $this->db->bind('xxx', $data['xxx']);
        // $this->db->bind('xxx', $xxx);
        // return $this->db->resultSet();
    }
}

// QUERY TEMPLATE
/*
$sql = "";
$this->db->query($sql);
$this->db->bind();
$this->db->execute();
return $this->db->resultSet();
return $this->db->resultOne();
*/
