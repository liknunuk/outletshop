<?php
class Model_supplyer
{
    private $table = "msg_supplyer";
    // Columns: namaSupplyer , telepon, idSupplyer
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }


    // Basic CRUD OPERATION //

    // INSERTION
    public function tambah($data)
    {
        $sql = "INSERT INTO $this->table SET namaSupplyer=:namaSupplyer , telepon=:telepon, idSupplyer=:idSupplyer";
        $this->db->query($sql);
        $this->db->bind('namaSupplyer', $data['namaSupplyer']);
        $this->db->bind('telepon', $data['telepon']);
        $this->db->bind('idSupplyer', NULL);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // UPDATE
    public function ngubah($data)
    {
        $sql = "UPDATE $this->table SET namaSupplyer=:namaSupplyer , telepon=:telepon WHERE idSupplyer=:idSupplyer";
        $this->db->query($sql);
        $this->db->bind('namaSupplyer', $data['namaSupplyer']);
        $this->db->bind('telepon', $data['telepon']);
        $this->db->bind('idSupplyer', $data['idSupplyer']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // DELETE
    public function sampah($data)
    {
        $sql = "DELETE FROM $this->table WHERE idSupplyer=:idSupplyer";
        $this->db->query($sql);
        $this->db->bind('idSupplyer', $data['idSupplyer']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // DISPLAY MULTIPLE
    public function tampil($pn = 1)
    {
        $row = ($pn - 1) * rows;
        $sql = "SELECT * FROM $this->table ORDER BY namaSupplyer LIMIT $row ," . rows;
        $this->db->query($sql);
        return $this->db->resultSet();
    }

    // DISPLAY SINGULAR
    public function detail($key)
    {
        $sql = "SELECT * FROM $this->table WHERE idSupplyer=:key ";
        $this->db->query($sql);
        $this->db->bind('key', $key);
        return $this->db->resultOne();
    }

    // CUSTOMIZED QUERY //
    public function findSupplyer($namaSupplyer)
    {
        $namaSupplyer = str_replace("-", " ", $namaSupplyer);
        $sql = "SELECT * FROM $this->table WHERE namaSupplyer LIKE :namaSupplyer ";
        $this->db->query($sql);
        $this->db->bind('namaSupplyer', "%{$namaSupplyer}%");
        return $this->db->resultSet();
    }
}

// QUERY TEMPLATE
/*
$sql = "";
$this->db->query($sql);
$this->db->bind();
$this->db->execute();
return $this->db->resultSet();
return $this->db->resultOne();
*/
