<?php
class Model_penjualan
{
    private $table = "msg_penjualan";
    // Invoice Columns : idInvoice , tanggal, idClient , grandTotal , bayar , kembali
    // penjualan Columns: idxPenjualan , idInvoice , idBarang , quantity , hargaJual
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    public function invoiceBaru(array $data)
    {
        $sql = "INSERT INTO msg_invoice SET tanggal=:tanggal , idClient=:idClient";
        $this->db->query($sql);
        $this->db->bind('tanggal', $data['tanggal']);
        $this->db->bind('idClient', $data['idClient']);
        $this->db->execute();
        return array($this->db->rowCount(), $this->db->lastId());
    }

    public function hargaPokok(string $idBarang)
    {
        $sql = "SELECT hargaJual harga FROM msg_barang WHERE idBarang=:idBarang";
        $this->db->query($sql);
        $this->db->bind('idBarang', $idBarang);
        return $this->db->resultOne();
    }

    public function setSales(array $data)
    {
        $hargaBeli = $this->hargaBeli($data['idBarang']);
        $sql = "INSERT INTO $this->table SET idInvoice=:idInvoice , idBarang=:idBarang , quantity=:quantity , hargaBeli=:hargaBeli, hargaJual=:hargaJual ";
        $this->db->query($sql);
        $this->db->bind('idInvoice', $data['idInvoice']);
        $this->db->bind('idBarang', $data['idBarang']);
        $this->db->bind('quantity', $data['quantity']);
        $this->db->bind('hargaBeli', $hargaBeli);
        $this->db->bind('hargaJual', $data['hargaJual']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function detailInvoice(int $idInvoice = null)
    {
        $sql = "SELECT msg_invoice.* , DATE_FORMAT(msg_invoice.tanggal,'%d/%m/%Y') tgInvoice , msg_client.namaClient, msg_client.tipeClient FROM msg_invoice , msg_client WHERE idInvoice=:idInvoice && msg_invoice.idClient = msg_client.idClient LIMIT 1";
        $this->db->query($sql);
        $this->db->bind('idInvoice', $idInvoice);
        return $this->db->resultOne();
    }

    public function dataInvoice($idInvoice)
    {
        $sql = "SELECT {$this->table}.* , namaBarang FROM $this->table, msg_barang WHERE idInvoice=:idInvoice && msg_barang.idBarang = {$this->table}.idBarang ORDER BY idxPenjualan";
        $this->db->query($sql);
        $this->db->bind('idInvoice', $idInvoice);
        return $this->db->resultSet();
    }

    public function fullInvoice($data)
    {

        $sql = "UPDATE msg_invoice SET grandTotal=:grandTotal , bayar=:bayar , kembali=:kembali , ongkir=:ongkir WHERE idInvoice=:idInvoice LIMIT 1";
        $this->db->query($sql);
        $this->db->bind('grandTotal', $data['grandTotal']);
        $this->db->bind('bayar', $data['bayar']);
        $this->db->bind('kembali', $data['kembali']);
        $this->db->bind('ongkir', $data['ongkir']);
        $this->db->bind('idInvoice', $data['idInvoice']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function invoiceCancel($data)
    {
        $sql = "DELETE FROM msg_penjualan WHERE idInvoice=:idInvoice";
        $this->db->query($sql);
        $this->db->bind('idInvoice', $data['idInvoice']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    private function hargaBeli(string $idBarang = NULL)
    {
        $sql = "SELECT hargaPokok FROM msg_barang WHERE idBarang=:idBarang LIMIT 1";
        $this->db->query($sql);
        $this->db->bind('idBarang', $idBarang);
        $result = $this->db->resultOne();
        return $result['hargaPokok'];
    }

    public function trxHistories($idClient = null)
    {
        $sql = "SELECT lpb.idInvoice , DATE_FORMAT(inv.tanggal,'%d/%m/%Y') tanggal , lpb.namaBarang , lpb.quantity , lpb.hargaJual, (lpb.quantity * lpb.hargaJual) jumlahHarga FROM labaPerBarang lpb , msg_invoice inv  WHERE lpb.idClient=:idClient  && lpb.idInvoice = inv.idInvoice ORDER BY lpb.idInvoice DESC LIMIT 180";
        $this->db->query($sql);
        $this->db->bind('idClient', $idClient);
        return $this->db->resultSet();
    }

    public function soldHistory(String $idBarang = null)
    {
        $sql = "SELECT lpb.idInvoice , DATE_FORMAT(inv.tanggal,'%d/%m/%Y') tanggal , lpb.namaClient , lpb.quantity , lpb.hargaJual, lpb.hargaBeli , (lpb.quantity * lpb.hargaJual) jumlahHarga FROM labaPerBarang lpb , msg_invoice inv  WHERE lpb.idBarang=:idBarang  && lpb.idInvoice = inv.idInvoice ORDER BY lpb.idInvoice DESC LIMIT 180";
        $this->db->query($sql);
        $this->db->bind('idBarang', $idBarang);
        return $this->db->resultSet();
    }

    public function cancelItem($data)
    {
        $sql = "DELETE FROM msg_penjualan WHERE idxPenjualan=:idxPenjualan ";
        $this->db->query($sql);
        $this->db->bind('idxPenjualan', $data['idxPenjualan']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function pendingItem($data)
    {
        $sql = "UPDATE msg_penjualan SET pending=:pending WHERE idxPenjualan=:idxPenjualan ";
        $this->db->query($sql);
        $this->db->bind('pending', $data['pending']);
        $this->db->bind('idxPenjualan', $data['idxPenjualan']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function depending($data)
    {
        list($inv, $idb, $pit) = explode("_", $data['par']);
        $sql = "UPDATE msg_penjualan SET pending=0 WHERE idInvoice =:inv && idBarang =:idb && pending=:pit LIMIT 1";
        $this->db->query($sql);
        $this->db->bind('inv', $inv);
        $this->db->bind('idb', $idb);
        $this->db->bind('pit', $pit);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function invoiceSeek($data)
    {
        $sql = "SELECT idInvoice , tanggal , msg_client.idClient , namaClient FROM msg_invoice , msg_client WHERE tanggal=:tanggal &&  (msg_invoice.idClient LIKE :iklien || namaClient LIKE :nklien) && msg_client.idClient = msg_invoice.idClient";

        $this->db->query($sql);
        $this->db->bind('tanggal', $data['tanggal']);
        $this->db->bind('iklien', "%{$data['klien']}%");
        $this->db->bind('nklien', "%{$data['klien']}%");
        return $this->db->resultSet();
    }

    // Daftar Invoice - expedisi
    public function invopedisi($pn = 1)
    {
        $row = ($pn - 1) * rows;
        $sql = "SELECT idInvoice , DATE_FORMAT(tanggal , '%d - %m - %Y') tanggal, namaClient , expedisi, waybill FROM msg_invoice , msg_client WHERE msg_client.idClient = msg_invoice.idClient ORDER BY idInvoice DESC LIMIT $row," . rows;
        $this->db->query($sql);
        return $this->db->resultSet();
    }

    public function setEkspedisi($data)
    {
        $sql = "UPDATE msg_invoice SET expedisi =:expedisi , waybill=:waybill WHERE idInvoice=:idInvoice";
        $this->db->query($sql);
        $this->db->bind('expedisi', $data['expedisi']);
        $this->db->bind('waybill', $data['waybill']);
        $this->db->bind('idInvoice', $data['idInvoice']);
        $this->db->execute();
        return $this->db->rowCount();
    }
}

// QUERY TEMPLATE
/*
$sql = "";
$this->db->query($sql);
$this->db->bind();
$this->db->execute();
return $this->db->resultSet();
return $this->db->resultOne();
*/
