<?php
class Model_template
{
    private $table = "namaTabel";
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }


    // Basic CRUD OPERATION //

    // INSERTION
    public function tambah($data)
    {
        $sql = "INSERT INTO $this->table SET ";
        $this->db->query($sql);
        $this->db->bind('xxx', $data['xxx']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // UPDATE
    public function ngubah($data)
    {
        $sql = "UPDATE $this->table SET WHERE ";
        $this->db->query($sql);
        $this->db->bind('xxx', $data['xxx']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // DELETE
    public function sampah($data)
    {
        $sql = "DELETE FROM $this->table WHERE ";
        $this->db->query($sql);
        $this->db->bind('xxx', $data['xxx']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // DISPLAY MULTIPLE
    public function tampil($pn = 1)
    {
        $row = ($pn - 1) * rows;
        $sql = "SELECT * FROM $this->table ORDER BY LIMIT $row ," . rows;
        $this->db->query($sql);
        return $this->db->resultSet();
    }

    // DISPLAY SINGULAR
    public function detail($key)
    {
        $sql = "SELECT * FROM $this->table WHERE xxx=:key ";
        $this->db->query($sql);
	$this->db->bind('key',$key);
        return $this->db->resultOne();
    }

    // CUSTOMIZED QUERY //
    public function something($data)
    {
        // $sql = "";
        // $this->db->query($sql);
        // $this->db->bind('xxx', $data['xxx']);
        // $this->db->bind('xxx', $xxx);
        // return $this->db->resultSet();
    }
}

// QUERY TEMPLATE
/*
$sql = "";
$this->db->query($sql);
$this->db->bind();
$this->db->execute();
return $this->db->resultSet();
return $this->db->resultOne();
*/
