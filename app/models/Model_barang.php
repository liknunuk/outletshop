<?php
class Model_barang
{
    private $table = "msg_barang";
    // Columns: idBarang , namaBarang , realStok , hargaPokok, hargaJual
    // idBarang bisa berupa barcode
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }


    // Basic CRUD OPERATION //

    // INSERTION
    public function tambah($data)
    {
        $sql = "INSERT INTO $this->table SET namaBarang=:namaBarang , hargaPokok=:hargaPokok , hargaJual=:hargaJual , idBarang=:idBarang ";
        $this->db->query($sql);
        $this->db->bind('namaBarang', $data['namaBarang']);
        $this->db->bind('hargaPokok', $data['hargaPokok']);
        $this->db->bind('hargaJual', $data['hargaJual']);
        $this->db->bind('idBarang', $data['idBarang']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // UPDATE
    public function ngubah($data)
    {
        $sql = "UPDATE $this->table SET namaBarang=:namaBarang , hargaPokok=:hargaPokok, hargaJual=:hargaJual  WHERE idBarang=:idBarang";
        $this->db->query($sql);
        $this->db->bind('namaBarang', $data['namaBarang']);
        $this->db->bind('hargaPokok', $data['hargaPokok']);
        $this->db->bind('hargaJual', $data['hargaJual']);
        $this->db->bind('idBarang', $data['idBarang']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // DELETE
    public function sampah($data)
    {
        $sql = "DELETE FROM $this->table WHERE idBarang=:idBarang";
        $this->db->query($sql);
        $this->db->bind('idBarang', $data['idBarang']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // DISPLAY MULTIPLE
    // public function tampil($pn = 1)
    // {
    //     $row = ($pn - 1) * rows;
    //     $sql = "SELECT {$this->table}.* ,SUM(msg_penjualan.pending) pending FROM {$this->table} , msg_penjualan WHERE msg_penjualan.idBarang = {$this->table}.idBarang GROUP BY {$this->table}.idBarang LIMIT $row ," . rows;
    //     $this->db->query($sql);
    //     return $this->db->resultSet();
    // }

    public function tampil($pn = 1)
    {
        $row = ($pn - 1) * rows;
        $sql = "SELECT * FROM $this->table ORDER BY namaBarang LIMIT $row ," . rows;
        $this->db->query($sql);
        return $this->db->resultSet();
    }

    // DISPLAY SINGULAR
    public function detail($key)
    {
        $sql = "SELECT * FROM $this->table WHERE idBarang=:key ";
        $this->db->query($sql);
        $this->db->bind('key', $key);
        return $this->db->resultOne();
    }

    // CUSTOMIZED QUERY //
    public function cari($data)
    {
        // $data = str_replace("-", " ", $data);
        $sql = "SELECT * FROM $this->table WHERE namaBarang LIKE :data LIMIT " . rows;
        $this->db->query($sql);
        $this->db->bind('data', "%{$data}%");
        return $this->db->resultSet();
    }

    public function realStok(string $idBarang = NULL)
    {
        $sql = "SELECT realStok FROM $this->table WHERE idBarang LIKE :idBarang LIMIT 1 ";
        $this->db->query($sql);
        $this->db->bind("idBarang", $idBarang);
        return $this->db->resultOne();
    }

    public function restoking($data)
    {
        $sql = "UPDATE $this->table SET realStok=:realStok  WHERE idBarang=:idBarang";
        $this->db->query($sql);
        $this->db->bind('realStok', $data['realStok']);
        $this->db->bind('idBarang', $data['idBarang']);
        $this->db->execute();
        return $this->db->rowCount();
    }
}

// QUERY TEMPLATE
/*
$sql = "";
$this->db->query($sql);
$this->db->bind();
$this->db->execute();
return $this->db->resultSet();
return $this->db->resultOne();
*/
