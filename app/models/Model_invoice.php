<?php
class Model_template
{
    private $table = "msg_invoice";
    // Columns Invoice:  tanggal, idClient , grandTotal , bayar , kembali, idInvoice
    // Columns Penjualan: idInvoice , idBarang , quantity , hargaJual, idxPenjualan
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }


    // Basic CRUD OPERATION //

    // INSERTION
    public function tambah($data)
    {
        $sql = "INSERT INTO $this->table SET tanggal=:tanggal, idClient=:tanggal , idInvoice=:idInvoice";
        $this->db->query($sql);
        $this->db->bind('tanggal', $data['tanggal']);
        $this->db->bind('idClient', $data['idClient']);
        $this->db->bind('idInvoice', $data['idInvoice']);
        $this->db->execute();
        return array($this->db->rowCount(), $this->db->lastId());
    }


    // UPDATE
    public function ngubah($data)
    {
        $sql = "UPDATE $this->table SET tanggal=:tanggal, idClient=:tanggal WHERE idInvoice=:idInvoice";
        $this->db->query($sql);
        $this->db->bind('tanggal', $data['tanggal']);
        $this->db->bind('idClient', $data['idClient']);
        $this->db->bind('idInvoice', $data['idInvoice']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // DELETE
    public function sampah($data)
    {
        $sql = "DELETE FROM $this->table WHERE idInvoice=:idInvoice";
        $this->db->query($sql);
        $this->db->bind('idInvoice', $data['idInvoice']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // DISPLAY MULTIPLE
    public function tampil($pn = 1)
    {
        $row = ($pn - 1) * rows;
        $sql = "SELECT * FROM $this->table ORDER BY idInvoice Desc LIMIT $row ," . rows;
        $this->db->query($sql);
        return $this->db->resultSet();
    }

    // DISPLAY SINGULAR
    public function detail($key)
    {
        $sql = "SELECT * FROM $this->table WHERE idInvoice=:key ";
        $this->db->query($sql);
        $this->db->bind('key', $key);
        return $this->db->resultOne();
    }

    // CUSTOMIZED QUERY //
    public function something($data)
    {
        // $sql = "";
        // $this->db->query($sql);
        // $this->db->bind('xxx', $data['xxx']);
        // $this->db->bind('xxx', $xxx);
        // return $this->db->resultSet();
    }
}

// QUERY TEMPLATE
/*
$sql = "";
$this->db->query($sql);
$this->db->bind();
$this->db->execute();
return $this->db->resultSet();
return $this->db->resultOne();
*/
