<?php
class Model_client
{
    private $table = "msg_client";
    // Columns: namaClient, telepon, tipeClient, idClient
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }


    // Basic CRUD OPERATION //

    // INSERTION
    public function tambah($data)
    {
        $sql = "INSERT INTO $this->table SET namaClient=:namaClient, telepon=:telepon, tipeClient=:tipeClient, idClient=:idClient";
        $this->db->query($sql);
        $this->db->bind('namaClient', $data['namaClient']);
        $this->db->bind('telepon', $data['telepon']);
        $this->db->bind('tipeClient', $data['tipeClient']);
        $this->db->bind('idClient', $data['idClient']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // UPDATE
    public function ngubah($data)
    {
        $sql = "UPDATE $this->table SET namaClient=:namaClient, telepon=:telepon, tipeClient=:tipeClient WHERE idClient=:idClient";
        $this->db->query($sql);
        $this->db->bind('namaClient', $data['namaClient']);
        $this->db->bind('telepon', $data['telepon']);
        $this->db->bind('tipeClient', $data['tipeClient']);
        $this->db->bind('idClient', $data['idClient']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // DELETE
    public function sampah($data)
    {
        $sql = "DELETE FROM $this->table WHERE idClient=:idClient";
        $this->db->query($sql);
        $this->db->bind('idClient', $data['idClient']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // DISPLAY MULTIPLE
    public function tampil($pn = 1)
    {
        $row = ($pn - 1) * rows;
        $sql = "SELECT msg_client.* , (SELECT SUM(pending) FROM msg_penjualan , msg_invoice WHERE msg_penjualan.idInvoice = msg_invoice.idInvoice && msg_invoice.idClient = msg_client.idClient) pending FROM msg_client LIMIT {$row}," . rows;
        // $sql = "SELECT * FROM $this->table ORDER BY namaClient LIMIT $row ," . rows;
        $this->db->query($sql);
        return $this->db->resultSet();
    }

    // DISPLAY SINGULAR
    public function detail($key)
    {
        $sql = "SELECT * FROM $this->table WHERE idClient=:key ";
        $this->db->query($sql);
        $this->db->bind('key', $key);
        return $this->db->resultOne();
    }

    // CUSTOMIZED QUERY //
    public function clientByCat($data = "Member")
    {
        $sql = "SELECT * FROM $this->table WHERE tipeClient=:data";
        $this->db->query($sql);
        $this->db->bind('data', $data);
        return $this->db->resultSet();
    }

    public function clientByName($data)
    {
        $data = str_replace("-", " ", $data);
        $sql = "SELECT * FROM $this->table WHERE namaClient LIKE :data LIMIT " . rows;
        $this->db->query($sql);
        $this->db->bind('data', "%{$data}%");
        return $this->db->resultSet();
    }
}

// QUERY TEMPLATE
/*
$sql = "";
$this->db->query($sql);
$this->db->bind();
$this->db->execute();
return $this->db->resultSet();
return $this->db->resultOne();
*/
