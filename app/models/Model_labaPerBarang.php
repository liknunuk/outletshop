<?php
class Model_labaPerBarang
{
    private $table = "labaPerBarang";
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    // Kontribusi member dan reseller
    public function kontribusi(String $tipe = "", String $bulan = null)
    {
        $bulan = $bulan == NULL ? date('Y-m') . "%" : $bulan . "%";
        $sql = "SELECT lpb.idClient , lpb.namaClient , SUM(laba) kontribusi , client.tipeClient FROM labaPerBarang lpb, msg_client client WHERE tanggal LIKE :bulan && client.tipeClient =:tipe && lpb.idClient = client.idClient GROUP BY lpb.idClient ORDER BY client.tipeClient , lpb.namaClient";

        $this->db->query($sql);
        $this->db->bind('bulan', $bulan);
        $this->db->bind('tipe', $tipe);
        return $this->db->resultSet();
    }

    public function lajuHarian(string $tp = '%', string $tanggal = NULL, string $ampai = NULL)
    {
        $tanggal = $tanggal == NULL ? date('Y-m-d') : $tanggal;
        $sql = "SELECT tanggal, idInvoice, idBarang , namaBarang, quantity , pending , hargaBeli, hargaJual , ( hargaJual * quantity ) subtotal , laba, tipeClient FROM labaPerBarang, msg_client WHERE msg_client.idClient = labaPerBarang.idClient && tanggal BETWEEN :tanggal AND :ampai && tipeClient LIKE :tp ORDER BY idInvoice, namaBarang";
        $this->db->query($sql);
        $this->db->bind('tanggal', $tanggal);
        $this->db->bind('ampai', $ampai);
        $this->db->bind('tp', $tp);
        return $this->db->resultSet();
    }

    public function topSale(string $bulan = NULL)
    {
        $bulan = $bulan == NULL ? date('Y-m') . "%" : $bulan . "%";
        $sql = "SELECT namaBarang, sum(quantity) quantity FROM labaPerBarang WHERE tanggal LIKE :bulan GROUP BY  idBarang ORDER BY quantity DESC";
        $this->db->query($sql);
        $this->db->bind('bulan', $bulan);
        return $this->db->resultSet();
    }
}

// QUERY TEMPLATE
/*
$sql = "";
$this->db->query($sql);
$this->db->bind();
$this->db->execute();
return $this->db->resultSet();
return $this->db->resultOne();
*/
