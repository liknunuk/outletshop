<?php
// Columns: namaClient, telepon, tipeClient, idClient
class Clients extends Controller
{
    public function __construct()
    {
        if (!isset($_SESSION['ugroup']) && !isset($_SESSION['userid'])) {
            header("Location:" . BASEURL . "Home/logout");
        }
    }

    // method default
    public function index($pn = 1)
    {
        /*
        bc = barcode
        pn = page number
         */

        $data = [
            'title' => 'Manajemen Client',
            'clients' => $this->model('Model_client')->tampil($pn),
            'fmod' => 'new',
            'pn' => $pn,
        ];

        $this->view('template/header', $data);
        $this->view('manajemen/mgmtHead');
        $this->view('manajemen/client', $data);
        $this->view('template/footer');
    }

    public function simpan()
    {
        // print_r($_POST);
        // exit();
        if ($_POST['fmod'] == 'new') {
            $this->setClient($_POST);
        } else {
            $this->chgClient($_POST);
        }
    }

    private function setClient($data)
    {
        echo $this->model('Model_client')->tambah($data) > 0 ? "1" : "0";
    }

    private function chgClient($data)
    {
        echo $this->model('Model_client')->ngubah($data) > 0 ? "1" : "0";
    }

    public function afkir()
    {
        echo $this->model('Model_client')->sampah($_POST) > 0 ? "1" : "0";
    }

    public function loClients($pn = 1)
    {
        $data = $this->model('Model_client')->tampil($pn);
        echo json_encode($data);
    }

    public function cari(string $nama)
    {
        $data = $this->model('Model_client')->clientByName($nama);
        echo json_encode($data);
    }

    public function trxes(string $idClient = NULL)
    {
        $data = [
            'client' => $this->model('Model_client')->detail($idClient),
            'trxes' => $this->model('Model_penjualan')->trxHistories($idClient),
            'title' => 'Riwayat Transaksi'
        ];
        $this->view('template/header', $data);
        $this->view('manajemen/mgmtHead');
        $this->view('manajemen/transaksi', $data);
        $this->view('template/footer');
    }

    public function myInvoice(int $id = null)
    {
        $data = [
            'invoice' => $this->model('Model_penjualan')->detailInvoice($id),
            'barang' => $this->model('Model_penjualan')->dataInvoice($id)
        ];
        $this->view('manajemen/myinvoice', $data);
    }
}
