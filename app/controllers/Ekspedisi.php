<?php
class Ekspedisi extends Controller
{
    public function __construct()
    {
        if (!isset($_SESSION['ugroup']) && !isset($_SESSION['userid'])) {
            header("Location:" . BASEURL . "Home/logout");
        }
    }

    public function index($pn = 1)
    {
        $data = [
            'invoices' => $this->model('Model_penjualan')->invopedisi($pn),
            'title' => 'Data Pengiriman',
            'expeditor' => $this->expeditor()
        ];
        $this->view('template/header', $data);
        $this->view('sales/salesHead');
        $this->view('sales/invopedisi', $data);
        $this->view('template/footer');
    }

    public function setEkspedisi()
    {
        if ($this->model('Model_penjualan')->setEkspedisi($_POST) > 0) {
            Alert::setAlert('berhasil diupdate', 'Data Pengiriman', 'success');
        } else {
            Alert::setAlert('gagal diupdate', 'Data Pengiriman', 'danger');
        }
        header("Location:" . BASEURL . "Ekspedisi");
    }

    public function expeditor()
    {
        $expeditor = [
            ['nama' => 'JNE', 'track' => 'https://www.jne.co.id/id/tracking/trace'],
            ['nama' => 'ANTERAJA', 'track' => 'https://anteraja.id/tracking'],
            ['nama' => 'POS INDONESIA', 'track' => 'https://www.posindonesia.co.id/id/tracking'],
            ['nama' => 'JNT', 'track' => 'https://jet.co.id/track'],
            ['nama' => 'TIKI', 'track' => 'https://www.tiki.id/id/tracking'],
            ['nama' => 'COD', 'track' => ''],
        ];
        asort($expeditor);
        return $expeditor;
    }
}
