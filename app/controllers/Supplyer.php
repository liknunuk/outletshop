<?php
// Columns: namaSupplyer , telepon, idSupplyer
class Supplyer extends Controller
{
    public function __construct()
    {
        if (!isset($_SESSION['ugroup']) && !isset($_SESSION['userid'])) {
            header("Location:" . BASEURL . "Home/logout");
        }
    }

    // method default
    public function index($pn = 1)
    {
        /*
        bc = barcode
        pn = page number
         */

        $data = [
            'title' => 'Manajemen Supplyer',
            'supplyer' => $this->model('Model_supplyer')->tampil($pn),
            'fmod' => 'new',
            'pn' => $pn,
        ];

        $this->view('template/header', $data);
        $this->view('manajemen/mgmtHead');
        $this->view('manajemen/supplyer', $data);
        $this->view('template/footer');
    }

    public function simpan()
    {

        if ($_POST['fmod'] == 'new') {
            $this->setSupplyer($_POST);
        } else {
            $this->chgSupplyer($_POST);
        }
    }

    private function setSupplyer($data)
    {
        echo $this->model('Model_supplyer')->tambah($data) > 0 ? "1" : "0";
    }

    private function chgSupplyer($data)
    {
        echo $this->model('Model_supplyer')->ngubah($data) > 0 ? "1" : "0";
    }

    public function afkir()
    {
        echo $this->model('Model_supplyer')->sampah($_POST) > 0 ? "1" : "0";
    }

    public function loSupplyer($pn = 1)
    {
        $data = $this->model('Model_supplyer')->tampil($pn);
        echo json_encode($data);
    }

    public function cari(string $nama)
    {
        $data = $this->model('Model_supplyer')->findSupplyer($nama);
        echo json_encode($data);
    }
}
