<?php
// Columns: idBarang , namaBarang , realStok , hargaPokok, hargaJual
class Barang extends Controller
{
    public function __construct()
    {
        if (!isset($_SESSION['ugroup']) && !isset($_SESSION['userid'])) {
            header("Location:" . BASEURL . "Home/logout");
        }
    }

    // method default
    public function index($pn = 1)
    {
        /*
        bc = barcode
        pn = page number
         */

        $data = [
            'title' => 'Manajemen Barang',
            'barang' => $this->model('Model_barang')->tampil($pn),
            'fmod' => 'new',
            'pn' => $pn,
        ];

        $this->view('template/header', $data);
        $this->view('manajemen/mgmtHead');
        $this->view('manajemen/barang', $data);
        $this->view('template/footer');
    }

    public function simpanBarang()
    {
        if ($_POST['fmod'] == 'new') {
            $this->setBarang($_POST);
        } else {
            $this->chgBarang($_POST);
        }
    }

    private function setBarang($data)
    {
        echo $this->model('Model_barang')->tambah($data) > 0 ? "1" : "0";
    }

    private function chgBarang($data)
    {
        echo $this->model('Model_barang')->ngubah($data) > 0 ? "1" : "0";
    }

    public function afkir()
    {
        echo $this->model('Model_barang')->sampah($_POST) > 0 ? "1" : "0";
    }

    public function loBarang($pn = 1)
    {
        $data = $this->model('Model_barang')->tampil($pn);
        echo json_encode($data);
    }

    public function cari(String $namaBarang)
    {
        $barang = str_replace("-", " ", $namaBarang);
        $data = $this->model('Model_barang')->cari($barang);
        echo json_encode($data);
    }

    // Manajemen Pembelian
    public function pembelian($pn = 1)
    {
        $data = [
            'title' => 'Manajemen Pembelian',
            'pn' => $pn,
            'fmod' => 'new',
            'pembelian' => $this->model('Model_pembelian')->tampil($pn)
        ];

        $this->view('template/header', $data);
        $this->view('manajemen/mgmtHead');
        $this->view('manajemen/beli', $data);
        $this->view('template/footer');
    }

    // simpan pembelian
    public function savePembelian()
    {
        if ($_POST['fmod'] == 'new') {
            echo $this->model('Model_pembelian')->tambah($_POST) > 0 ? "1" : "0";
        } else if ($_POST['fmod'] == 'chg') {
            echo $this->model('Model_pembelian')->ngubah($_POST) > 0 ? "1" : "0";
        } else {
            echo "0";
        }
    }

    // batal beli
    public function rasidatuku()
    {
        echo $this->model('Model_pembelian')->sampah($_POST) > 0 ? "1" : "0";
    }

    // filter pembelian
    public function filterbeli($col, $val)
    {
        $data = $this->model('Model_pembelian')->filterbeli($col, $val);
        echo json_encode($data);
    }

    public function cadapem($col, $val)
    {
        $val = str_replace("-", " ", $val);
        $data = $this->model('Model_pembelian')->beliby($col, $val);
        echo json_encode($data);
    }

    // pembelian terakhir
    public function lastkulak($pn = 1)
    {
        $data = $this->model('Model_pembelian')->tampil($pn);
        echo json_encode($data);
    }

    public function dari(string $idSupplyer)
    {
        $data = $this->model('Model_pembelian')->beliDari($idSupplyer);
        echo json_encode($data);
    }

    public function byBarang(string $idBarang)
    {
        $data = $this->model('Model_pembelian')->beliBarang($idBarang);
        echo json_encode($data);
    }

    public function byInvoice(string $idInvoice)
    {
        $data = $this->model('Model_pembelian')->byInvoice($idInvoice);
        echo json_encode($data);
    }

    public function soldHistory($idBarang)
    {
        $idBarang = str_replace("-", " ", $idBarang);
        $data = [
            'title' => 'Riwayat Penjualan Item',
            'items' => $this->model('Model_penjualan')->soldHistory($idBarang),
            'barang' => $this->model('Model_barang')->detail($idBarang)
        ];
        $this->view('template/header', $data);
        $this->view('manajemen/mgmtHead');
        $this->view('manajemen/soldItem', $data);
        $this->view('template/footer');
    }

    public function restoking()
    {
        // print_r($_POST);
        echo $this->model('Model_barang')->restoking($_POST) > 0 ? "1" : "0";
    }
}
