<?php

class Users extends Controller
{
    public function __construct()
    {
        if (!isset($_SESSION['ugroup']) && !isset($_SESSION['userid'])) {
            header("Location:" . BASEURL . "Home/logout");
        }
    }

    public function index(int $pn = 1)
    {
        $data = [
            'title' => 'Users',
            'pn' => $pn
        ];

        $this->view('template/header', $data);
        $this->view('manajemen/mgmtHead');
        $this->view('manajemen/users', $data);
        $this->view('template/footer');
    }

    public function loUsers($pn = 1)
    {
        $data = $this->model('Model_users')->tampil($pn);
        echo json_encode($data);
    }

    public function setUser()
    {
        if ($_POST['fmod'] == 'new') {
            $this->addUser($_POST);
        }

        if ($_POST['fmod'] == 'chg') {
            // print_r($_POST);
            $this->chgUser($_POST);
        }

        if ($_POST['fmod'] == 'rmv') {
            $this->rmvUser($_POST);
        }

        if ($_POST['fmod'] == 'pwd') {
            $this->pwdUser($_POST);
        }
    }

    private function addUser(array $data)
    {
        if ($this->model('Model_users')->tambah($data) > 0) {
            Alert::setAlert('berhasil ditambahkan', 'Data user', 'success');
        } else {
            Alert::setAlert('gagal ditambahkan', 'Data user', 'danger');
        }
        header("Location:" . BASEURL . "Users");
    }

    private function chgUser(array $data)
    {
        if ($this->model('Model_users')->ngubah($data) > 0) {
            Alert::setAlert('berhasil diupdate', 'Data user', 'success');
        } else {
            Alert::setAlert('gagal diupdate', 'Data user', 'danger');
        }
        header("Location:" . BASEURL . "Users");
    }

    private function rmvUser(array $data)
    {
        echo $this->model('Model_users')->sampah($data) > 0 ? "1" : "0";
    }

    private function pwdUser(array $data)
    {
        if ($this->model('Model_users')->changeKey($data) > 0) {
            Alert::setAlert('berhasil diupdate', 'Data password', 'success');
        } else {
            Alert::setAlert('gagal diupdate', 'Data password', 'danger');
        }
        header("Location:" . BASEURL . "Users");
    }
}
