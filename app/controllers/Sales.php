<?php
// sesuaikan nama kelas, tetap extends ke Controller
class Sales extends Controller
{
    public function __construct()
    {
        if (!isset($_SESSION['ugroup']) && !isset($_SESSION['userid'])) {
            header("Location:" . BASEURL . "Home/logout");
        }
    }
    // method default
    public function index()
    {
        $data = [
            'title' => 'Sales Service'
        ];
        $this->view('template/header', $data);
        $this->view('sales/salesHead');
        $this->view('sales/index', $data);
        $this->view('template/footer');
    }

    public function newInvoice()
    {
        $invoice = $this->model('Model_penjualan')->invoiceBaru($_POST);
        echo $invoice[0] > 0 ? sprintf("%04d", $invoice[1]) : "0";
    }

    public function tipeclient(string $idClient)
    {
        $data = $this->model('Model_client')->detail($idClient);
        echo $data['tipeClient'];
    }

    public function hargaJual(string $idBarang)
    {
        $idBarang = str_replace("-", " ", $idBarang);
        $harga = $this->jualHarga($idBarang);
        $mxqty = $this->jualMxQty($idBarang);
        $data = array('hargaJual' => $harga, 'realStok' => $mxqty);
        echo json_encode($data);
    }

    private function jualHarga(string $idBarang)
    {
        $data = $this->model('Model_penjualan')->hargaPokok($idBarang);
        return $data['harga'];
    }

    private function jualMxQty(string $idBarang)
    {
        $data = $this->model('Model_barang')->realStok($idBarang);
        return $data['realStok'];
    }

    public function caderang(string $nb)
    {
        $nb = str_replace("-", " ", $nb);
        $data = $this->model('Model_barang')->cari($nb);
        echo json_encode($data);
    }

    public function setSales()
    {
        echo $this->model('Model_penjualan')->setSales($_POST) > 0 ? "1" : "0";
    }

    public function invoiceInfo($idInvoice)
    {
        $data = $this->model('Model_penjualan')->detailInvoice($idInvoice);
        echo json_encode($data);
    }

    public function invoiceData($idInvoice)
    {
        $data = $this->model('Model_penjualan')->dataInvoice($idInvoice);
        echo json_encode($data);
    }

    public function closing()
    {
        // print_r($_POST);
        echo $this->model('Model_penjualan')->fullInvoice($_POST) > 0 ? "1" : "0";
    }

    public function cancelItem()
    {
        echo $this->model('Model_penjualan')->cancelItem($_POST) > 0 ? "1" : "0";
    }

    public function pendingItem()
    {
        echo $this->model('Model_penjualan')->pendingItem($_POST) > 0 ? "1" : "0";
    }

    public function depending()
    {
        echo $this->model('Model_penjualan')->depending($_POST) > 0 ? "1" : "0";
    }

    public function invoice(string $idInvoice)
    {
        // echo $idInvoice;
        $data = [
            'invoice' => $this->model('Model_penjualan')->detailInvoice($idInvoice),
            'barang' => $this->model('Model_penjualan')->dataInvoice($idInvoice),
        ];
        $this->view('sales/invoice', $data);
    }

    public function invoiceSeek()
    {
        $data = $this->model('Model_penjualan')->invoiceSeek($_POST);
        echo json_encode($data);
    }

    public function cancelInvoice()
    {
        // print_r($_POST);
        // echo trim($_POST['idInvoice']);
        echo $this->model('Model_penjualan')->invoiceCancel($_POST) > 0 ? "1" : "0";
    }

    public function kontribusi(String $kategori = NULL, String $bulan = null)
    {
        $bulan = $bulan == NULL ? date('Y-m') : $bulan;
        $kategori = $kategori == NULL ? "User" : $kategori;
        $data = [
            'laba' => $this->model('Model_labaPerBarang')->kontribusi($kategori, $bulan),
            'title' => 'Kontribusi Klien',
            'bulan' => $bulan,
            'kategori' => $kategori
        ];
        $this->view('template/header', $data);
        $this->view('sales/salesHead');
        $this->view('sales/monthlyshare', $data);
        $this->view('template/footer');
    }

    public function topSale(String $bulan = null)
    {
        $bulan = $bulan == NULL ? date('Y-m') : $bulan;
        $data = [
            'sale' => $this->model('Model_labaPerBarang')->topSale($bulan),
            'title' => 'Top Sale',
            'bulan' => $bulan
        ];
        $this->view('template/header', $data);
        $this->view('sales/salesHead');
        $this->view('sales/topsale', $data);
        $this->view('template/footer');
    }

    public function laju(string $tp = "semua", string $tanggal = NULL, string $ampai = NULL)
    {
        $tc = $tp == "semua" ? "%" : $tp;
        $tanggal = $tanggal == NULL ? date('Y-m-d') : $tanggal;
        $ampai = $ampai == NULL ? $tanggal : $ampai;
        $data = [
            'title' => "Laporan Penjualan {$tanggal}",
            'tanggal' => $tanggal,
            'sampai' => $ampai,
            'klien' => $tp,
            'penjualan' => $this->model('Model_labaPerBarang')->lajuHarian($tc, $tanggal, $ampai)
        ];
        $this->view('template/header', $data);
        $this->view('sales/salesHead');
        $this->view('sales/lajuh', $data);
        $this->view('template/footer');
    }
}
