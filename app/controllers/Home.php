<?php
// sesuaikan nama kelas, tetap extends ke Controller
class Home extends Controller
{

  // method default
  public function index()
  {
    $data = [
      'Title' => 'MS Glow Banjarnegara',
    ];
    $this->view('template/header', $data);
    $this->view('home/index');
    $this->view('template/footer');
  }

  public function auth()
  {

    $auth = $this->model('Model_users')->authenticate($_POST);
    // 7bcffabcf3e48be9e7e328c9459620bb

    if ($auth == true) {
      $_SESSION['ugroup'] = $auth['userGroup'];
      $_SESSION['ufname'] = $auth['namaLengkap'];
      $_SESSION['userid'] = $auth['idUser'];

      if ($auth['userGroup'] != 'frontOffice') {
        header('Location:' . BASEURL . "Barang");
      } elseif ($auth['userGroup'] == 'frontOffice') {
        header('Location:' . BASEURL . "Sales");
      } else {
        header('Location:' . BASEURL . "Home/logout");
      }
    } else {
      // echo "Auth False";
      // exit();
      Alert::setAlert('tidak ditemukan', 'Data user', 'danger');
      header('Location:' . BASEURL);
    }
  }

  public function logout()
  {
    session_unset();
    session_destroy();
    header("Location:" . BASEURL);
  }
}
