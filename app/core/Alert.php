<?php

class Alert
{
    public static function setAlert($pesan , $operasi , $tipe){

        $_SESSION['alert'] = [
            'pesan'  => $pesan,
            'operasi'=> $operasi,
            'warna'  => $tipe
        ];
    }
    
    public static function sankil(){
        if ( isset($_SESSION['alert'])){
            
            echo '
            <div class="alert alert-'.$_SESSION['alert']['warna'].' alert-dismissible fade show" role="alert">
                ' . $_SESSION['alert']['operasi'] . ' <strong>'.$_SESSION['alert']['pesan'].'</strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            ';

            unset($_SESSION['alert']);
            
        }
    }
}